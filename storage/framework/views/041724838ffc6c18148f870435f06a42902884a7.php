
<!DOCTYPE html> 
<html lang="en">
    <head>
        <meta charset="utf-9" />

        <title>ierek shear</title>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" />
       <?php echo $__env->make('website.includes.css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    </head>
    <body>
        
        <!-- Modal  Login-->
        <div class="modal fade" id="m_login" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
          <div class="modal-dialog col-12  col-md-9 mx-auto modal-dialog-scrollable" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Login</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
             
                <form method="post" class="row mx-0">
                   <div class="col-12">
                   
                        <!-- ----Login----- -->
                       <div class="form-group">
                        <label for="email-1">Email</label>
                        <input type="email" class="form-control" id="email-1" aria-describedby="emailHelp" placeholder="Enter email">

                       </div>
            
                       <!-- ----Password----- -->
                       <div class="form-group">
                        <label for="pass-1">Password</label>
                        <input type="password" class="form-control" id="pass-1" placeholder="Password">
                      </div>
                   </div>
                   
                   <!-----BTN Login--------->
                   <div class="modal-footer col-12 py-0 mt-5">
                       <button type="submit" class="btn btn-primary">Login</button>
                   </div> 
                </form>
             
            </div>
            </div>
          </div>
        </div>
        
        <!-- Modal  SignUp-->
        <div class="modal fade row mx-0" id="m_signup" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
          <div class="modal-dialog col-12  col-md-8 mx-auto modal-dialog-scrollable" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Register</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
               <form method="post" class="row mx-0">


                    <!------FirstName--------- -->
                  <div class="form-group col-md-6 col-12 ">
                    <label for="s_first">First Name</label>
                    <input type="email" class="form-control" id="s_first" placeholder="First Name"  />

                   </div>
                   
                   <!--------Last Name------- -->
                  <div class="form-group col-md-6 col-12">
                    <label for="s_last">Last Name</label>
                    <input type="email" class="form-control" id="s_last" placeholder="Last Name" />

                   </div>
                   
                   <!-- -----Email---------- -->
                  <div class="form-group col-12">
                    <label for="s_emial">Email</label>
                    <input type="email" class="form-control" id="s_emial" placeholder="Email" />

                   </div>                   
                   

                   <!-- ----PassWord------ -->
                  <div class="form-group col-12">
                    <label for="s_pass">Password</label>
                    <input type="password" class="form-control" id="s_pass" placeholder="Password" />
                  </div>
                      <div class="modal-footer col-12 py-0 mt-5">
                          <button type="submit" class="btn btn-primary">Create My Account</button>
                      </div>             
                  </form>
              </div>

            </div>
          </div>
        </div>
    
        
        <!-- _______Header___________ -->
<?php echo $__env->make('website.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <!-- _______intro _Home__________ -->
        <sction class="slider_cs w-100">
            <div class="full-inner full-inner-two">
                <div class="con_slider text-center px-4">
                    <!-- Title Intro Home  -->
                    <h1 class="h1">IEREK</h1>
                    
                    <!-- ____Title One______ -->
                    <h2 class="h2">Academic Research Community</h2>
                    <p class="lead text-justify">EREK Press is a multidisciplinary publisher that aims to cultivate and disseminate research. It takes pride in being an influencing part of the academic research community that seeks to enlighten the minds of its readers. We are an international establishment that publishes academic journals in all aspects of Science, Engineering, and technology. Our duty is to encourage scholars, students, and educational institutions to join us in this journey of knowledge enrichment.</p>
                </div>  
            </div>

        </sction>

        <!-- ________Articale Section________________ -->
        <section class="w-100 py-5">

            <div class="container-fluid clearfix">
                <div class="de_full de_view">
                    <h2 class="w-100 font-heading h1 col-12">Academic Research </h2>
                    <!-------Script Code !!!------------>
                    <div  class="col-12 col-md-12 col-lg-6 col-xl-4 g_one"></div>
                    <!-------Script Code !!!------------>
                    
                        <div class="row mx-0">
                            
         

                       
                                     <?php $__currentLoopData = $publications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $publication): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>    
                                    <article class="arti_profile col-12 col-md-auto">
                                        <a href="<?php echo e($publication->link); ?>" class="coll_for_u row mx-0">
                                            <div class="col-4 d-flex justify-content-center align-items-center">

                                                <img class="img-fluid" src="<?php echo e(asset('assets/website/img/item-2.png')); ?>  " />
                                            </div>
                                            <div class="col-8 coll_con ">
                                                <h6 class="h6 mb-1"><?php echo e($publication->title); ?></h6>
                                                <p class="small "><?php echo e($publication->publisher); ?></p> 
                                                <p class="small m-0"><?php echo e($publication->date); ?></p>
                
                                            </div>
                                        </a>

                                    </article>
                                    
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      
                            
                            
                 
                                
                       </div>  
                       <div class="w-100 pt-5 text-center">
                           <a href="./academic_research.html" class="btn-save col-12 col-md-4 col-lg-3 mx-auto d-inline-block">Read More <i class="fas fa-angle-double-right"></i></a>
                       </div>
                </div>

            </div>
        </section>

        <!-- __________Mobile Apps__________ -->
        <!-- <section class="w-100 py-4">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-md-6"></div>
                    <div class="col-12 col-md-6"></div>
                </div>
            </div>
        </section> -->

        <!-- ______Partners________Recent Styles________ -->
        <section class="w-100 py-4 se_slider">
            <div class="container-fluid clearfix">
                <div class="de_full de_view">

                    <!-- _________Slider Recent Styles__________ -->
                    <h2 class="w-100 h1 pt-5  mb-5 font-heading">Recent Styles</h2>
                    <div class="col-12 mx-auto mt-5 pb-4">
                        <div class="owl-carousel owl-theme">

                            <div> <a class="slide_image" href="#"><img  src="<?php echo e(asset('assets/website/img/journalThumbnail_en_US.png')); ?>" /></a> </div>
                            <div> <a class="slide_image" href="#"><img  src="<?php echo e(asset('assets/website/img/journalThumbnail_en_US (1).png')); ?>" /></a> </div>
                            
                            <div> <a class="slide_image" href="#"><img  src="<?php echo e(asset('assets/website/img/journalThumbnail_en_US (2).png')); ?>" /></a> </div>
                            
                            <div> <a class="slide_image" href="#"><img  src="<?php echo e(asset('assets/website/img/journalThumbnail_en_US.png')); ?>" /></a> </div>
                            
                            <div> <a class="slide_image" href="#"><img  src="<?php echo e(asset('assets/website/img/journalThumbnail_en_US (1).png')); ?>" /></a> </div>
                           
                            <div> <a class="slide_image" href="#"><img  src=" <?php echo e(asset('assets/website/img/journalThumbnail_en_US (2).png')); ?>" /></a> </div>
                        </div>
                    </div>


                    <!------------______________--------->
                    <div class="w-100 font-heading h1 mt-5 mb-5 col-12">Partners</div>
                    <div class="row mx-0 text-center">

                        <div class="col-sm-4 mx-auto col-md-3 col-6"><div class="img-arti"><img src="<?php echo e(asset('assets/website/img/f5e2b03fc225d92519dc0ee4943d0b23cdaf9e04F1F2.png')); ?>" /></div></div>
                        
                        <div class="col-sm-4 mx-auto col-md-3 col-6"> <div class="img-arti"><img src="<?php echo e(asset('assets/website/img/046bb72ba4e0575965d464c0b2778414ad294e00EC92.png')); ?>" /></div></div>
                        
                       
                        <div class="col-sm-4 mx-auto col-md-3 col-6"><div class="img-arti"><img src="<?php echo e(asset('assets/website/img/22583a258be086a30879ca0399a47f0d0a6239bb9325.png')); ?>" /></div></div>
                    
                        <div class="col-sm-4 mx-auto col-md-3 col-6"><div class="img-arti"><img src="<?php echo e(asset('assets/website/img/314d1443b51bef1f70fedc2aa3cd5e529b0072f065E3.png')); ?> " /></div></div>
                     

                     
                        
                    </div>
  
                </div>
            </div>
        </section>

 <?php echo $__env->make('website.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<!-- END FOOTER -->
		<!-- End footer -->

<?php echo $__env->make('website.includes.js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </body> 
</html>