<!DOCTYPE html> 
<html lang="en">
    <head>
        <meta charset="utf-9" />

        <title>My Profile</title>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" />
       
       <?php echo $__env->make('website.includes.css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    </head>
    <body> 

        <!-----Header------>
<?php echo $__env->make('website.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



        <!---Section-------->
        <section class="w-100 py-5 main_screen my_profile_maine">

            <div class="container clearfix px-0 px-md-3">
                <div class="de_full de_view">
                    
                    
                    <!-------Script Code !!!------------>
                    <div  class="col-12 col-md-12 col-lg-6 col-xl-4 g_one"></div>
                    <!-------Script Code !!!------------>
                    
                    
                    <div class="col-12 pb-5 px-0 px-md-3">
                        <div class="inp_pub p-3  row mx-0">
                            <div class="col-12 pt-2 col-lg-3">
                                <div class="user-pro">
                                    <img src="<?php echo e(Request::root()); ?>/public/uplodes/userimage/<?php echo e($profiloes->image); ?>" />
                                </div>
                            </div>   

                            <div class="col-12 col-lg-9 pt-4 pt-lg-2 d-flex align-items-center">
                                <div class="head_profile col-12 p-0">
                                    <h2 class="h2 pb-3 m-0"><span class="sup_title">DR :</span> <?php echo e(Auth::guard('web')->user()->name); ?></h2>
                                    <div class="row mx-0 bo_top pt-3">
                                        <ul class="list_ul col-12 col-lg-4">
                                            <li><?php echo e($profiloes->institutionalaffiliation); ?></li>
                                            <li>Lecturer, Analytics</li>
                                            <li><?php echo e($profiloes->country); ?></li>
                                        </ul>

                                        <ul class="list_ul list_ul_a  col-12 col-lg-8">
        
                                            <li><a class="a_list tw_a" href="<?php echo e($findme->twitter); ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                            <li><a class="a_list fc_a" href="<?php echo e($findme->facebook); ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a class="a_list in_a" href="<?php echo e($findme->linkedin); ?>" target="_blank"><i class="fab fa-linkedin-in" target="_blank"></i></a></li>
                                            <li><a class="a_list ube_a" href="<?php echo e($findme->youtube); ?>" target="_blank"><i class="fab fa-youtube"></i></a></li>
                                   
                                        </ul>                                            
                                    </div>

                                </div>
                            </div>

                        <!--     <div class="col-12 bo_top my-5 py-4">
                                <h4 class="h4 color_app_text_1"><i class="fas fa-book"></i> My co-authors include</h4>
                                <div class="row pt-4">
                                    <div class="col-12 col-md-auto py-2">
                                        <a href="#" class="for_authors w-100" >
                                            <div class="photo_authors">
                                                <img src="img/avtar.png" />
                                            </div>
                                            Some Name
                                        </a>
                                    </div>
                                    <div class="col-12 col-md-auto py-2">
                                            <a href="#" class="for_authors w-100" >
                                                <div class="photo_authors">
                                                    <img src="img/avtar.png" />
                                                </div>
                                                Some Name
                                            </a>
                                        </div>

                                        <div class="col-12 col-md-auto py-2">
                                                <a href="#" class="for_authors w-100" >
                                                    <div class="photo_authors">
                                                        <img src="img/avtar.png" />
                                                    </div>
                                                    Some Name
                                                </a>
                                            </div>    
                                </div>
                            </div>

 -->

                            <div class="col-12 bo_top  mt-5 py-4">
                                <h4 class="h4 color_app_text_1"><i class="fas fa-archive"></i> My Publications</h4>
                                <div class="row py-5 ">
                                     <?php $__currentLoopData = $publications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $publication): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>    
                                    <article class="arti_profile col-12 col-md-auto">
                                        <a href="<?php echo e($publication->link); ?>" class="coll_for_u row mx-0">
                                            <div class="col-4 d-flex justify-content-center align-items-center">

                                                <img class="img-fluid" src="<?php echo e(asset('assets/website/img/item-2.png')); ?>" />
                                            </div>
                                            <div class="col-8 coll_con ">
                                                <h6 class="h6 mb-1"><?php echo e($publication->title); ?></h6>
                                                <p class="small "><?php echo e($publication->publisher); ?></p> 
                                                <p class="small m-0"><?php echo e($publication->date); ?></p>
                
                                            </div>
                                        </a>

                                    </article>
                                    
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                               
                                                                        
                                </div>




                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>


 <?php echo $__env->make('website.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- END FOOTER -->
        <!-- End footer -->

<?php echo $__env->make('website.includes.js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </body> 
</html>