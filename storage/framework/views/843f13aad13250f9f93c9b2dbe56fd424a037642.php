<!DOCTYPE html> 
<html lang="en">
    <head>
        <meta charset="utf-9" />

        <title>Authors</title>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" />
        
        <meta property="og:title" content="European Travel Destinations">
<meta property="og:description" content="Offering tour packages for individuals or groups.">
<meta property="og:image" content="http://euro-travel-example.com/thumbnail.jpg">
<meta property="og:url" content="http://euro-travel-example.com/index.htm">

       
         <?php echo $__env->make('website.includes.css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    </head>
    <body>
  
             <!-- _______Header___________ -->
<?php echo $__env->make('website.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <!---Section-------->
        <section class="w-100 py-5 main_screen my_profile_maine">

            <div class="container clearfix px-0 px-md-3">
                <div class="de_full de_view">
                    
                    
                    <!-------Script Code !!!------------>
                    <div  class="col-12 col-md-12 col-lg-6 col-xl-4 g_one"></div>
                    <!-------Script Code !!!------------>
                    
                    
                    <div class="col-12 pb-5 px-0 ">
                        <div class="inp_pub p-3  row mx-0">
                            <h4 class="h4 color_app_text_1 py-3"><i class="fas fa-box"></i>Explain</h4>

                            <form class="col-12 bo_top py-4" action="<?php echo e(url('authors_update').'/'.$publications->puid); ?>" method="POST">
                            	<?php echo e(csrf_field()); ?>

                                <p class="color_app_text_1 py-3">Explain your work in plain language to make it easier for readers to find, understand, and apply your work. For reference, you can review your publication/abstract <a href="" class="a_colors">here.</a></p>
                            
                                <div class="w-100 div_area">
                                    <h3 class="head_area m-0 py-2 px-3">Plain language title</h3>
                                    <textarea class="form-control" rows="5"  placeholder="Add a plain language title to make your publication easier to find and to help increase citations. Please note there is a limit of 100 characters." name="plain"><?php echo e($publications->plaintitle); ?>

                                    </textarea>
                                    <div class="w-100 px-3">
                                        <div class="w-100 clearfix">                         
                                            <div class="log_div float-right" onclick="toList('t_a_1','i_a_1', 'i_a_1')"> 
                                                See examples
                                                <span id="i_a_1">
                                                    <i class="fas fa-angle-right"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="w-100 d_drop" id="t_a_1">
                                            <p>Add a plain language title to make your publication easier to find and to help increase citations. Please note there is a limit of 100 characters.</p>
                                            <h6>Example 1</h6>
                                            <p>California's coastal plants build sand dunes through teamwork</p>
                                            <h6>Example 2</h6>
                                            <p>Social strategies of baboons</p>
                                            <h6>Example 3</h6>
                                            <p>History of parenting in England</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="w-100 div_area">
                                    <h3 class="head_area m-0 py-2 px-3">What is it about?</h3>
                                    <textarea class="form-control" rows="5"  placeholder="Add a simple, non-technical explanation or the lay summary of your publication to make it more accessible to a broader audience. Please note that this is not the abstract, but a plain language summary to help more people find and understand your work." name="about"><?php echo e($publications->about); ?></textarea>
                                    <div class="w-100 px-3">
                                        <div class="w-100 clearfix">                         
                                            <div class="log_div float-right" onclick="toList('t_a_2','i_a_2', 'i_a_2')"> 
                                                See examples
                                                <span id="i_a_2">
                                                    <i class="fas fa-angle-right"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="w-100 d_drop" id="t_a_2">
                                            <p>Add a plain language title to make your publication easier to find and to help increase citations. Please note there is a limit of 100 characters.</p>
                                            <h6>Example 1</h6>
                                            <p>California's coastal plants build sand dunes through teamwork</p>
                                            <h6>Example 2</h6>
                                            <p>Social strategies of baboons</p>
                                            <h6>Example 3</h6>
                                            <p>History of parenting in England</p>
                                        </div>
                                    </div>
                                </div>       
                                    
                                <div class="w-100 div_area">
                                    <h3 class="head_area m-0 py-2 px-3">Why is it important?</h3>
                                    <textarea class="form-control" rows="5"  placeholder="Add an explanation of what is unique and/or timely about your work, and the difference it might make to help increase readership." name="important"><?php echo e($publications->important); ?></textarea>
                                    <div class="w-100 px-3">
                                        <div class="w-100 clearfix">                         
                                            <div class="log_div float-right" onclick="toList('t_a_3','i_a_3', 'i_a_3')"> 
                                                See examples
                                                <span id="i_a_3">
                                                    <i class="fas fa-angle-right"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="w-100 d_drop" id="t_a_3">
                                            <p>Add a plain language title to make your publication easier to find and to help increase citations. Please note there is a limit of 100 characters.</p>
                                            <h6>Example 1</h6>
                                            <p>California's coastal plants build sand dunes through teamwork</p>
                                            <h6>Example 2</h6>
                                            <p>Social strategies of baboons</p>
                                            <h6>Example 3</h6>
                                            <p>History of parenting in England</p>
                                        </div>
                                    </div>
                                </div>      
                                        
                                        
                                <div class="w-100 div_area">
                                    <h3 class="head_area m-0 py-2 px-3">Perspectives</h3>
                                    <textarea class="form-control" rows="5"  placeholder="Add a plain language title to make your publication easier to find and to help increase citations. Please note there is a limit of 100 characters." name="perspectives"><?php echo e($publications->Perspectives); ?></textarea>
                                    <div class="w-100 px-3">
                                        <div class="w-100 clearfix">                         
                                            <div class="log_div float-right" onclick="toList('t_a_4','i_a_4', 'i_a_4')"> 
                                                See examples
                                                <span id="i_a_4">
                                                    <i class="fas fa-angle-right"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="w-100 d_drop" id="t_a_4">
                                            <p>Add a plain language title to make your publication easier to find and to help increase citations. Please note there is a limit of 100 characters.</p>
                                            <h6>Example 1</h6>
                                            <p>California's coastal plants build sand dunes through teamwork</p>
                                            <h6>Example 2</h6>
                                            <p>Social strategies of baboons</p>
                                            <h6>Example 3</h6>
                                            <p>History of parenting in England</p>
                                        </div>
                                    </div>
                                </div>     
                                
                                <div class="col-12 text-center">
                                    <button class="btn w-50 btn-save"><i class="fas fa-sd-card"></i>Save</button>      
                                </div>
                            </form>

                            <!-- Image -->
                            <h4 class="h4 color_app_text_1 py-3"><i class="fas fa-images"></i>Image</h4>
                            <form class="col-12 bo_top py-4">
                                    <div class="w-100 div_area pb-3">
                                        <h3 class="head_area m-0 py-2 px-3">Add a featured image</h3>
                                        <p class="w-100 pt-3 px-3">Select a featured image for your publication. This will help your publication stand out and help showcase your work.</p>
                                        <form class="row mx-0">
                                            <div class="col-12 col-md-12 col-lg-8 d-flex  justify-content-center align-items-center">
                                                    <div class="form-group mt-0 for_ed_user w-100">
                                                            <input type="search" class="form-control" placeholder="Type one or two keywords" />
                                                            <button class="btn"><i class="fas fa-search"></i></button>
                                                    </div>
                                            </div>
                                            <div class="col-12 col-md-12 col-lg-4">
                                                <div class="div-images">
                                                    <img class="img-fluid" src="img/item-2.png" />
                                                </div>
                                            </div>
                                            <div class="row mx-0 mt-5">
                                                <div class="col-4 col-md-4 col-lg-3 py-2">
                                                    <div class="flow_photo">
                                                        <img class="img-fluid" src="img/item-2.png" />
                                                    </div>
                                                </div>
                                                    
                                                <div class="col-4 col-md-4 col-lg-3 py-2">
                                                    <div class="flow_photo">
                                                        <img class="img-fluid" src="img/item-2.png" />
                                                    </div>
                                                </div>
                                                    
                                                <div class="col-4 col-md-4 col-lg-3 py-2">
                                                    <div class="flow_photo">
                                                        <img class="img-fluid" src="img/item-2.png" />
                                                    </div>
                                                </div>
                                                            
                                                <div class="col-4 col-md-4 col-lg-3 py-2">
                                                    <div class="flow_photo">
                                                        <img class="img-fluid" src="img/item-2.png" />
                                                    </div>
                                                </div>
                                                    
                                                <div class="col-4 col-md-4 col-lg-3 py-2">
                                                    <div class="flow_photo">
                                                        <img class="img-fluid" src="img/item-2.png" />
                                                    </div>
                                                </div>
             
                                            </div>
                                            <div class="w-100 p-3 text-center"><button class="btn w-50 btn-save"><i class="fas fa-sd-card"></i>Save</button></div>

                                        </form>
 
                                    </div>
        
                            </form>

                            <!-- SHear -->
                            <h4 class="h4 color_app_text_1 py-3"><i class="fas fa-share-alt"></i>Share</h4>
                            <div class="col-12 bo_top py-4">
                                    <div class="w-100 div_area">
                                        <h3 class="head_area m-0 py-2 px-3">Share</h3>
                                        <p class="w-100 pt-3 px-3">Select a channel to share your work on :</p>
                                        <div class="w-100 px-3">
                                            <button class="link_d color_links" id="btn_a_1" onclick="a_links('btn_a_1', 'd_sh_1')" href="#">
                                                <span class="span_d"><i class="share-icon fa fa-link"></i></span>
                                                
                                                Link
                                            </button>

                                            <button class="link_d color_twit" id="btn_a_2" onclick="a_links('btn_a_2', 'd_sh_2')">
                                                    <span class="span_d"><i class="fab fa-twitter"></i></span>
                                                    
                                                    Twitter
                                            </button>

                                            <button class="link_d color_in" id="btn_a_3" onclick="a_links('btn_a_3', 'd_sh_3')" href="">
                                                    <span class="span_d"> <i class="fab fa-linkedin-in"></i></span>
                                                        
                                                    Linkedin
                                            </button>


                                            <button class="link_d color_fc" id="btn_a_4" onclick="a_links('btn_a_4', 'd_sh_4')">
                                                    <span class="span_d"><i class="fab fa-facebook-f"></i></span>
                                                        
                                                        Facebook
                                            </button>  

                        



                                            

                                        </div>
                                        <div class="w-100 block_v p-3">

                                            <!-------Shear Link----------->
                                            <div class="w-100 shear_link div-shear " id="d_sh_1"> 

                                                    <form class="row mx-0">
                                                        <div class="col-12 py-3 px-1 d-flex align-items-center">
                                                            <input type="text" class="form-control text-center inpt_gro" id="myLinks" value="http://ierek.eu/author/ierek/<?php echo e($publications->puid); ?>" readonly />
                                                        </div>
        
                                                        <div class="col-12 text-center px-1">
     <div onclick="eve_copy()" class="btn btn-save w-50 float-right">Copy Link</div>
      <a target="_blank" href="mailto:?subject=See%20my%20latest%20publication%20on%20Kudos&body=See%20my%20latest%20publication%20here:%0Dhttp://ierek.eu/author/ierek/<?php echo e($publications->puid); ?>" class="btn w-50 copy_role buo_a w-50" > Share by email</a>

     
                                                        </div>
                                                    </form>
                                            </div>

                                            <!-------Shear To Twitter----------->
                                            <div class="w-100 shear_twitter div-shear" id="d_sh_2">

                                                    <h6 class="mt-2">Share to Twitter</h6>
                                                    <p class="small">Create a trackable link that you can share online or offline - for example, by email; in a presentation, poster or handout; on a website, blog or via social media. You can give your shares a label (not publicly visible) to help you compare results.</p>
                                                
                                                    <div class="col-12 p-0">
                                                        <a href="https://twitter.com/intent/tweet?text=http://ierek.eu/author/ierek/<?php echo e($publications->puid); ?>" target="_blank" class="btn w-100 btn-save" ata-show-count="false">Confirm identity</a>
                                                    </div>
                                                    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                                            </div>

                                            <!-------Shear To Linkedin---------->
                                            <div class="w-100 shear_linkedin div-shear" id="d_sh_3">

                                                    <h6 class="mt-2">Share to LinkedIn</h6>
                                                    <p class="small">Create a trackable link that you can share online or offline - for example, by email; in a presentation, poster or handout; on a website, blog or via social media. You can give your shares a label (not publicly visible) to help you compare results.</p>
        
                                                    <div class="col-12 p-0"  data-href="https://www.linkedin.com/shareArticle?mini=true&url=http://ierek.eu/author/ierek/<?php echo e($publications->puid); ?>" data-layout="button_count" data-size="small" >
                                                        <a href="https://www.linkedin.com/shareArticle?mini=true&url=http://ierek.eu/author/ierek/<?php echo e($publications->puid); ?>" target="_blank" class="btn w-100 btn-save">Confirm identity</a>
                                                    </div>
                                            </div>


                                            <!-------Shear To FaceBook---------->
                                            <div class="w-100 shear_linkedin div-shear" id="d_sh_4">

                                                    <h6 class="mt-2">Share to Facebook</h6>
                                                    <p class="small">Create a trackable link that you can share online or offline - for example, by email; in a presentation, poster or handout; on a website, blog or via social media. You can give your shares a label (not publicly visible) to help you compare results.</p>
        
                                                    <div class="col-12 p-0" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-size="small">
                                                        <a href="https://www.facebook.com/sharer/sharer.php?u=http://ierek.eu/author/ierek/<?php echo e($publications->puid); ?>" target="_blank" class="btn w-100 btn-save">Confirm identity</a>
                                                    </div>
                                            </div>                                            
                                        </div>
                                    </div>
        
                            </div>



                            
                            <h4 class="h4 color_app_text_1 py-3"><i class="fas fa-book"></i>Resources</h4>
                            <div class="col-12 bo_top py-4">
                                    <div class="w-100 div_area">
                                        <h3 class="head_area m-0 py-2 px-3">Links to online resources</h3>
                                        <p class="w-100 p-3">External resources such as presentations, videos, interviews, figures, data-sets or related publications</p>
                                       	<form class="w-100 p-3 d-none" id="line_divi" action="<?php echo e(url('addresources').'/'.$publications->puid); ?>" method="POST">
                                            <?php echo e(csrf_field()); ?>

										  <div class="form-group">
										    <label for="reference_1">Resource type </label>
											  <select  name="resource_type" class="custom-select" id="reference_1" required>
											    <option selected>Choose...</option>
											    <option value="1">One</option>
											    <option value="2">Two</option>
											    <option value="3">Three</option>
											  </select>
										  </div>

										  <div class="form-group">
										    <label for="re-title">Resource title</label>
										    <input type="text" name="resource_title" class="form-control" id="re-title" placeholder="Resource title" required>
										  </div>

										  <div class="form-group">
										    <label for="resource-url">Resource URL (must start with http:// or https://)</label>
										    <input type="text" name="resource_url" class="form-control" id="resource-url" placeholder="resource-url" required>
										  </div>							

										  <div class="form-group">
										    <label for="resource_des">Resource description</label>
  											<textarea  name="resource_description" class="form-control" id="resource-id" row="7"  placeholder="Resource description" required></textarea>
										  </div>	
											  <div class="w-100 clearfix text-center">
												<button type="submit" class="btn btn-save w-50 float-right">Save</button>
											  	<div class="btn buo_a w-50" onclick="ve_see_2('line_divi', 'b_divi')">Cansel</div>
											  	
											</div>		  	  
                                       	</form>
                                        <div class="w-100 p-3">
                                        	<div class="btn w-100 btn-save" id="b_divi" onclick="ve_see('line_divi', 'b_divi')"> <i class="fa fa-plus"></i>Add resource</div>
                                        </div>
                                    </div>
        
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>


 <?php echo $__env->make('website.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- END FOOTER -->
        <!-- End footer -->

<?php echo $__env->make('website.includes.js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    </body> 
</html>