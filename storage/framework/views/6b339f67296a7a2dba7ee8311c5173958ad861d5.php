


<!DOCTYPE html> 
<html lang="en">
    <head>
        <meta charset="utf-9" />

        <title>Publications</title>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" />
  <?php echo $__env->make('website.includes.css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    </head>
    <body>
<?php echo $__env->make('website.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


        <!---Section-------->
        <section class="w-100 py-5 main_screen publication_main">

            <div class="container clearfix px-0 px-md-3">
                <div class="de_full de_view">
                    
                    
                    <!-------Script Code !!!------------>
                    <div  class="col-12 col-md-12 col-lg-6 col-xl-4 g_one"></div>
                    <!-------Script Code !!!------------>
                    
                    
                    <form class="col-12 pb-5 px-0 px-md-3">
                        <div class="w-100 inp_pub py-3 row  text-md-right dir_for_grid mx-0">
                            <div class="col-12 col-md-12 py-2 col-lg-10 col-xl-9 mx-auto text-left dir_for_grid_en">
                                    <h4 class="h4 color_app_text_1 dir_for_grid_en pb-5">
                                        <i class="fas fa-archive"></i> Add Publications 
                                        <a href="addpublication" class="btn buo_a mt-3 mt-md-0" ><i class="fas fa-book"></i> Add Publications</a>
                                    </h4>

                                <div class="input-group bo_top">
                                  <input type="search" class="form-control" placeholder="Search title" aria-label="Recipient's username" aria-describedby="button-addon2">
                                  <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" id="button-addon2"><i class="fas fa-search"></i></button>
                                  </div>
                                </div>
                            </div>
                            
                            
                            <div class="col-12 col-md-12 py-5 col-lg-10 col-xl-9 mx-auto text-left dir_for_grid_en">
                                <div class="w-100 row py-2">
                                  <label class="col-12 c col-md-12 col-lg-4"><span>Preference :</span></label>
                                    <div class="col-6 col-md-6 col-lg-4">
                                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                            <option selected>Choose...</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                    
                                    <div class="col-6 col-md-6 col-md-6 col-lg-4">
                                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                            <option selected>Choose...</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>

                                
                                <div class="w-100 row py-2">
                                  <label class="col-12 col-md-12 col-lg-4"><span>Show publications from :</span></label>
                                    <div class="col-12 col-md-12 col-lg-8">
                                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                            <option selected>Choose...</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                    
                                </div>
                                
                                
                                <div class="w-100 row py-2">
                                  <label class="col-12 c col-md-12 col-lg-4"><span>Published between:</span></label>
                                    <div class="col-6 col-md-6 col-lg-4">
                                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                            <option selected>Choose...</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                    
                                    <div class="col-6 col-md-6 col-md-6 col-lg-4">
                                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                            <option selected>Choose...</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>                                
                                
                            </div>

                            <div class="col-12 pt-5 py-2 col-lg-8 col-xl-7 mx-auto bo_top  text-center dir_for_grid_en">
                                <button class="btn go_btn"><i class="fas fa-filter"></i> Go</button>
                                <button class="btn reset_btn"> <i class="fas fa-sync-alt"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    
                    
                    <!-------Table------->
                    <div class="col-12 table-responsive px-0 px-md-3">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Publication Title</th>
                                    <th scope="col">Journal/Book</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">% Complete</th>
                                    <th scope="col">Kudos Views </th>
                                    <th scope="col">Citations*</th>
                                    <th scope="col">Altmetric*</th>
                                </tr>
                            </thead>
                            
                <?php $__currentLoopData = $publications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $publication): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tbody>
                                <tr>
 <th scope="row"><a href="<?php echo e(url('ierek')); ?>/<?php echo e($publication->puid); ?>"><?php echo e($publication->publisher); ?></a></th>
                                    <td><?php echo e($publication->title); ?></td>
                                    <td><?php echo e($publication->date); ?></td> 
                                    <td><?php echo e($publication->complete); ?></td>
                                    <td>8</td>
                                    <td>0</td>
                                    <td>-</td>
                                </tr>
                            </tbody>

             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </table> 
                    </div>
                </div>

            </div>
        </section>


 <?php echo $__env->make('website.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!-- END FOOTER -->
        <!-- End footer -->

<?php echo $__env->make('website.includes.js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </body> 
</html>