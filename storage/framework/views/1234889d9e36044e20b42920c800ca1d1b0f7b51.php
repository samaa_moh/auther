  <!-- ______Footer__________ -->
        <footer class="w-100 pt-5 pb-2">
            <div class="container-fluid clearfix">
                <div class="de_full de_view text-center text-sm-left">
                    <div class="row mx-0">
                        <div class="col-12  col-sm-6 col-md-4 col-lg-3">
                            <h4 class="foot_head">Educational Services</h4>
                            <ul>
                                <li><a href="">Conferences</a></li>
                                <li><a href="">Workshops</a></li>
                                <li><a href="">Study Abroad</a></li>
                            </ul>
                        </div>

                        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                            <h4 class="foot_head">Publishing Services</h4>
                            <ul>
                                <li><a href="">Book Series</a></li>
                                <li><a href="">IEREK Press</a></li>
                                <li><a href="">Scientific Committee</a></li>
                            </ul>
                        </div>
                            
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                            <h4 class="foot_head">Media</h4>
                            <ul>
                                <li><a href="">Blog</a></li>
                                <li><a href="">Announcements</a></li>
                            </ul>
                        </div>
                                
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                            <h4 class="foot_head">About</h4>
                            <ul>
                                <li><a href="">About us</a></li>
                                <li><a href="">FAQ</a></li>
                                <li><a href="">Contacts</a></li>
                            </ul>
                        </div>                                
                    </div>

                    
                    <div class="row mx-0 over_footer mt-5">
                        <div class="col-12 mb-3">
                            <div class="over_top"></div>
                        </div>
                        <div class="col text-left">
                            <a href=""><i class="fab fa-facebook-f"></i></a>
                            <a href=""><i class="fab fa-twitter"></i></a>
                            <a href=""><i class="fab fa-youtube"></i></a>
                            <a href=""><i class="fab fa-linkedin-in"></i></a>
                            <a href=""><i class="fab fa-instagram"></i></a>
                            <a href=""><i class="fab fa-android"></i></a>
                            <a href=""><i class="fab fa-apple"></i></a>
                        </div>
                        <div class="col text-center">
                            <p class="co_footer">© 2018 Copyright: <a href="">IEREK.COM</a></p>
                        </div>
                        <div class="col text-right">
                                <a class="" href="#">Careers</a>
                                <a class="" href="#">Suggest</a>
                                <a class="" href="#">Feedback</a>    
                        </div>
                    </div>
                </div>

            </div>
        </footer>