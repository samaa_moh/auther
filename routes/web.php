<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Psr\Container\ContainerInterface;
//use Auth;
use App\Notification;

Auth::routes(); 

Route::get('/home', 'HomeController@index')->name('home');

//for user routes
Route::get('/', function () {
    return view('website.index');
});


//SocialLite
Route::get('login/{provider}', 'AuthSocController@redirectToProvider');
Route::get('login/{provider}/callback', 'AuthSocController@handleProviderCallback');

/// USER Routes

Route::get('home/user/logout', function () {
    Auth()->guard('web')->logout();
    return redirect('/login');
});

///////////////////////////////////////////

//Admin Routes
Route::get('/admin/login', 'admin@index');
Route::post('/admin/login', 'admin@loginpost')->name('admin.login.submit');

Route::get('/admin/forget/password','admin@forgetpassword');
Route::get('/admin/reset/password/{token}','admin@resetpassword');
Route::post('/admin/reset/password/{token}','admin@postresetpassword');
Route::post('/admin/forget/password','admin@postforgetpassword');

Config::set('auth.defines','admin');
Route::get('admin/logout', function () {
    Auth()->guard('admin')->logout();
    return redirect('/admin/login');
});

///route website
Route::get('/','websitecontroller\home@index');


 

Route::get('gallery','websitecontroller\gallery@index');

Route::get('about','websitecontroller\about@index');

Route::get('services','websitecontroller\services@index');
Route::get('team','websitecontroller\teamController@index');
Route::get('career','websitecontroller\careerController@index');
Route::get('blog','websitecontroller\blog@index');
Route::get('contact','websitecontroller\contact@index');

Route::resource('/contactUs','OurcontactsController');

// Route::get('services', function () {
//     return view('website.services.index');
// });

Route::get('news','websitecontroller\news@index');

Route::get('contact','websitecontroller\contact@index');

///////////////////////////////////////////////////////////////////////////////

/// user route 
Route::group(['prefix' => LaravelLocalization::setLocale().'/','middleware'=>'auth:web'],function(){

Route::get('publication','websitecontroller\publication@index');
Route::get('addpublication','websitecontroller\addpublication@index');
Route::get('ierek/{puid}','websitecontroller\publication@edit');
Route::post('authors_update/{puid}','websitecontroller\publication@update');

Route::get('my/profile','websitecontroller\profile@index');
Route::get('my/sittings','websitecontroller\myprofile@index');

Route::post('addresources/{puid}','websitecontroller\addresources@edit');


//Route::POST('uplode','websitecontroller\profile@uplodeimage');


Route::post('my/editprofile','websitecontroller\profile@edit');
Route::post('my/editfindme','websitecontroller\profile@editfindme');


 });

///admin route
 Route::group(['prefix' => LaravelLocalization::setLocale().'/admin','middleware'=>'admin:admin'],function(){

        Route::get('','AdminController\AdminController@index')->name('admin.dashboard');
        //Slider Route
        Route::resource('slider','AdminController\slidercontroller');
        Route::get('slider/{id}/delete','AdminController\slidercontroller@destroy');

        //testimonial Route
         Route::resource('testimonial','AdminController\testimonialcontroller');
         Route::get('testimonial/{id}/delete','AdminController\testimonialcontroller@destroy');

         //team Route
         Route::resource('team','AdminController\teamcontroller');
         Route::get('team/{id}/delete','AdminController\teamcontroller@destroy');

         //team feature
         Route::resource('feature','AdminController\featurecontroller');
         Route::get('feature/{id}/delete','AdminController\featurecontroller@destroy');

         //blogs Route
         Route::resource('blogs','AdminController\blogcontroller');
         Route::get('blogs/{id}/delete','AdminController\blogcontroller@destroy');

         //about Route
         Route::resource('about','AdminController\aboutcontroller');
         Route::get('about/{id}/delete','AdminController\aboutcontroller@destroy');

         //services Route
         Route::resource('services','AdminController\servicescontroller');
         Route::get('services/{id}/delete','AdminController\servicescontroller@destroy');

         //contact Route
         Route::resource('contacts','AdminController\contactcontroller');
         Route::get('contacts/{id}/delete','AdminController\contactcontroller@destroy');

		 //caproject Route
         Route::resource('caproject','AdminController\caprojectcontroller');
         Route::get('caproject/{id}/delete','AdminController\caprojectcontroller@destroy');


        //project project
         Route::resource('project','AdminController\projectcontroller');
         Route::get('project/{id}/delete','AdminController\projectcontroller@destroy');

         //caproject Route
        Route::resource('cgallery','AdminController\cgalleryctcontroller');
        Route::get('cgallery/{id}/delete','AdminController\cgalleryctcontroller@destroy');


        //project project
        Route::resource('gallery','AdminController\gallerycontroller');
        Route::get('gallery/{id}/delete','AdminController\gallerycontroller@destroy');


        //gallery
        Route::resource('gallery','AdminController\gallerycontroller');
        Route::get('gallery/{id}/delete','AdminController\gallerycontroller@destroy');

        //Contact
        Route::resource('contactUs','OurcontactsController');
        Route::get('contactUs/{id}/delete','OurcontactsController@destroy');

 });
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//

Route::get('/ahmed',function (){
    dd(\Mcamara\LaravelLocalization\Facades\LaravelLocalization::getSupportedLocales());
});

Route::get('/order', function () {
    return view('front.order');
});

Route::get('/welcome/{locale}', function ($local) {
    \Illuminate\Support\Facades\App::setLocale($local);
    return view('welcome');
});

//////////////////////////////////////////
/*
 *
 *

Route::group(['middleware'=>'news'],function(){


Route::get('insert', 'newscontroller@showdb');
Route::post('insertdb', 'newscontroller@insertdb');
});



Route::get('send/mail', function () {
//Mail::to('ayousry943@gmail.com')->send(new App\Mail\testmail());
//	\App\Jobs\sendmailjob::dispatch();

	$job = (new \App\Jobs\sendmailjob)->delay(\Carbon\Carbon::now()->addSeconds(1));
	dispatch($job);
	return  'mail sent';
});

 */

//Route::get('data/user', function () {
//
//if (Gate::allows('showdata',auth()->user())) {
// return  view('welcome');
//}else{
//  return  'you dont have pertmation  ';
//}
//});
