<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationmetricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publicationmetrics', function (Blueprint $table) {
            $table->increments('id');
              $table->integer('publications_id')->unsigned();
            $table->foreign('publications_id')->references('id')->on('publications')->onDelete('cascade');
            $table->integer('numberofshares');
            $table->integer('clicksonshares');
            $table->integer('viewsonkudos');
            $table->integer('publicationbutton');
           

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publicationmetrics');
    }
}
