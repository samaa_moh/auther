<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()  
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('firstname')->nullable();
            $table->string('middlename')->nullable();
            $table->string('lastname')->nullable();
            $table->string('image')->nullable();
            $table->string('country')->nullable();
            $table->string('institutionalaffiliation')->nullable();
            
            

             $table->integer('roles_id')->nullable()->unsigned();
            $table->foreign('roles_id')->references('id')->on('roles')->onDelete('cascade');

            $table->integer('subjectareas_id')->nullable()->unsigned();
            $table->foreign('subjectareas_id')->references('id')->on('subjectareas')->onDelete('cascade');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
