<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('book')->nullable();
            $table->string('date')->nullable();
            $table->string('complete')->nullable();
            
            $table->text('plaintitle')->nullable();
            $table->text('about')->nullable();
            $table->text('important')->nullable();
            $table->text('Perspectives')->nullable();
            
            $table->string('link');
             $table->string('publisher')->nullable();
             $table->string('puid')->nullable();
         
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publications');
    }
}
