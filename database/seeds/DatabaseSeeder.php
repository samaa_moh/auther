<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


      //  \App\Models\Setting::insert(['namesetting'=>'Title','type'=>0]);
      //  \App\Models\Setting::insert(['namesetting'=>'Email','type'=>0]);
      //  \App\Models\Setting::insert(['namesetting'=>'Address','type'=>0]);
      //  \App\Models\Setting::insert(['namesetting'=>'Logo','type'=>2]);
      //  \App\Models\Setting::insert(['namesetting'=>'Favicon','type'=>2]);
      //  \App\Models\Setting::insert(['namesetting'=>'Phone','type'=>0]);
      //  \App\Models\Setting::insert(['namesetting'=>'Address','type'=>0]);
      //  \App\Models\Setting::insert(['namesetting'=>'Facebook','type'=>0]);
      //  \App\Models\Setting::insert(['namesetting'=>'LinkedIn','type'=>0]);
      //  \App\Models\Setting::insert(['namesetting'=>'Google_Plus','type'=>0]);
      //  \App\Models\Setting::insert(['namesetting'=>'Description','type'=>1]);


               App\Models\Admin::insert(['username' => 'admin','password' => bcrypt('123456'),'email' => 'admin@admin.com','name'=>'admin']);

// Student Permission
     App\Models\Permission::insert(['title'=>'اضافة الطالب','for'=>'الطالب']);
      App\Models\Permission::insert(['title'=>'تعديل الطالب','for'=>'الطالب']);
      App\Models\Permission::insert(['title'=>'حذف الطالب','for'=>'الطالب']);
      App\Models\Permission::insert(['title'=>'اضافة فاتورة الطالب','for'=>'الطالب']);
      App\Models\Permission::insert(['title'=>'اظهار الفاتورة','for'=>'الطالب']);

      App\Models\Permission::insert(['title'=>'اظهار الطالب','for'=>'الطالب']);

// Teacher Permission
      App\Models\Permission::insert(['title'=>'اضافة المدرس','for'=>'المدرس']);
      App\Models\Permission::insert(['title'=>'تعديل المدرس','for'=>'المدرس']);
      App\Models\Permission::insert(['title'=>'حذف المدرس','for'=>'المدرس']);

      App\Models\Permission::insert(['title'=>'اظهار المدرس','for'=>'المدرس']);

// Employee Permission
      App\Models\Permission::insert(['title'=>'اضافة الموظف','for'=>'الموظف']);
      App\Models\Permission::insert(['title'=>'تعديل الموظف','for'=>'الموظف']);
      App\Models\Permission::insert(['title'=>'حذف الموظف','for'=>'الموظف']);

    App\Models\Permission::insert(['title'=>'اظهار الموظف','for'=>'الموظف']);

// Education Year Permission
      App\Models\Permission::insert(['title'=>'اضافة المرحلة الدراسية','for'=>'المرحلة الدراسية']);
      App\Models\Permission::insert(['title'=>'تعديل المرحلة الدراسية','for'=>'المرحلة الدراسية']);
      App\Models\Permission::insert(['title'=>'حذف المرحلة الدراسية','for'=>'المرحلة الدراسية']);
          App\Models\Permission::insert(['title'=>'اظهار المرحلة الدراسية','for'=>'المرحلة الدراسية']);

//Class Room Permission
      App\Models\Permission::insert(['title'=>'اضافة السنة الدراسية','for'=>'السنة الدراسية']);
      App\Models\Permission::insert(['title'=>'تعديل السنة الدراسية','for'=>'السنة الدراسية']);
      App\Models\Permission::insert(['title'=>'حذف السنة الدراسية','for'=>'السنة الدراسية']);
      App\Models\Permission::insert(['title'=>'اظهار السنة الدراسية','for'=>'السنة الدراسية']);

//Subject Permission
      App\Models\Permission::insert(['title'=>'اضافة المادة الدراسية','for'=>'المادة الدراسية']);
      App\Models\Permission::insert(['title'=>'تعديل المادة الدراسية','for'=>'المادة الدراسية']);
      App\Models\Permission::insert(['title'=>'حذف المادة الدراسية','for'=>'المادة الدراسية']);
      App\Models\Permission::insert(['title'=>'اظهار المادة الدراسية','for'=>'المادة الدراسية']);

//Group Permission
      App\Models\Permission::insert(['title'=>'اضافة الجروب','for'=>'الجروب']);
      App\Models\Permission::insert(['title'=>'تعديل الجروب','for'=>'الجروب']);
      App\Models\Permission::insert(['title'=>'حذف الجروب','for'=>'الجروب']);

      App\Models\Permission::insert(['title'=>'اظهار الجروب','for'=>'الجروب']);

//Attendance Permission
      App\Models\Permission::insert(['title'=>'تسجيل الحضور للطلاب','for'=>'الحضور']);
      App\Models\Permission::insert(['title'=>'معرفة حضور الطلاب','for'=>'الحضور']);

      App\Models\Permission::insert(['title'=>'اظهار حضور الطلاب','for'=>'الحضور']);


        //Manger Permission
        App\Models\Permission::insert(['title'=>'اضافة ادمن','for'=>'الادمن']);
        App\Models\Permission::insert(['title'=>'تعديل ادمن','for'=>'الادمن']);
        App\Models\Permission::insert(['title'=>'حذف ادمن','for'=>'الادمن']);
        App\Models\Permission::insert(['title'=>'اظهار ادمن','for'=>'الادمن']);


        //Manger Permission
        App\Models\Permission::insert(['title'=>'اضافة وظيفة','for'=>'الوظائف']);
        App\Models\Permission::insert(['title'=>'تعديل وظيفة','for'=>'الوظائف']);
        App\Models\Permission::insert(['title'=>'حذف وظيفة','for'=>'الوظائف']);
        App\Models\Permission::insert(['title'=>'اظهار وظيفة','for'=>'الوظائف']);


        $role = new \App\Models\Role();
        $role->title = 'المدير';
        $role->save();


        $perm = \App\Models\Permission::all(['id'])->toArray();
        $role->permissions()->sync($perm);

        $admin = new \App\Models\Admin();
      //   $admin->username = 'yousry';
      //   $admin->password = bcrypt('4562008');
      //   $admin->email = 'yousry@admin.com';
      //   $admin->name = 'yousry';
            $admin->username = 'Muhammed';
            $admin->password = bcrypt('15201520');
            $admin->email = 'muhammed@admin.com';
            $admin->name = 'Muhammed';
            
             $admin->save();

             $admin->roles()->sync($role);

      //  App\Models\Permission::insert(['title'=>'Create Trip','for'=>'trip']);
      //  App\Models\Permission::insert(['title'=>'Edit Trip','for'=>'trip']);
      //  App\Models\Permission::insert(['title'=>'Delete Trip','for'=>'trip']);
      //  App\Models\Permission::insert(['title'=>'Create Category','for'=>'category']);
      //  App\Models\Permission::insert(['title'=>'Edit Category','for'=>'category']);
      //  App\Models\Permission::insert(['title'=>'Delete Category','for'=>'category']);
      //  App\Models\Permission::insert(['title'=>'Create Slider','for'=>'slider']);
      //  App\Models\Permission::insert(['title'=>'Edit Slider','for'=>'slider']);
      //  App\Models\Permission::insert(['title'=>'Delete Slider','for'=>'slider']);
      //  App\Models\Role::insert(['title' => 'المدير']);

          App\Models\Admin::insert(['username' => 'ahmed','password' => bcrypt('123456'),'email' => 'ahmed@ahmed.com','name'=>'admin']);


App\student::insert(['fname' => '_test_student','password' => bcrypt('123'),'remember_token' => bcrypt('123'),'email' => 'test@student.com','phone'=>'01276794081','class_room_id'=>1,'seg'=>'yousry@admin.com']);
App\User::insert(['name' => '_test_student','password' => bcrypt('123'),'remember_token' => bcrypt('123'),'email' => 'test@student.com']);

    }
}
