<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use App\caprojects;

class projects extends Model
{
  protected $table='projects';
  protected $fillable = [
    'title','description','photo',
  ];

  public function caprojects()
  {
       return $this->belongsTo(caprojects::class,'caprojects_id');
  }


}
