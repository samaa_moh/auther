<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class resources extends Model
{ 
    
   protected $table='resources';
  protected $fillable = [
      'resource_type','resource_title','resource_url','resource_description','publications_id',
  ];

  public function get_role()
  {
     
         return $this->belongsToMany('App\role','profiles','id','roles_id');



  }


}
