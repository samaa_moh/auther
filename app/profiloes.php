<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\role;
class profiloes extends Model
{
    

      protected $table='profiles';
  protected $fillable = [
      'title','firstname','middlename','lastname','subjectareas_id','image','roles_id',
  ];

  public function get_role()
  {
     
         return $this->belongsToMany('App\role','profiles','id','roles_id');



  }


  public function get_subjectareas()
  {
     
         
         
         return $this->belongsTo('App\subjectarea', 'subjectareas_id', 'id'); 



  }
  


}
