<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cagallery extends Model
{
  protected $table='cagalleries';
  protected $fillable = [
      'name',
  ];
}
