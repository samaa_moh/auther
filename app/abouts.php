<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class abouts extends Model
{
  protected $table='abouts';
  protected $fillable = [
      'id','text', 
  ];

  public function get_videos()
  {
      return $this->belongsTo(videos::class);
  }


}
