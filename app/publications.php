<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class publications extends Model
{
     protected $table='publications';
  protected $fillable = [
    'user_id','link','title','date','complete','publisher','puid','plaintitle','about','important','plaintitle','Perspectives',
  ];

  // public function caprojects()
  // {
  //      return $this->belongsTo(caprojects::class,'caprojects_id');
  // }
}
