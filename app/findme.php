<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class findme extends Model
{
     protected $table='findmes';
  protected $fillable = [
      'orcid','gate','academia','mendeley','twitter','facebook','linkedin','youtube','website','email','user_id',
  ];

  public function cgallerys()
  {
       return $this->belongsTo(cagallery::class,'cgalleries_id');
  }


} 
