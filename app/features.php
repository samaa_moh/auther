<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class features extends Model
{
  protected $table='features';
  protected $fillable = [
      'title','photo','description',
  ];
 
  public function get_category()
  {
      return $this->belongsTo(videos::class);
  }


}
