<?php

namespace App\Helper;

class IceHelper{


    function getSetting($settingname = 'Title'){
        return \App\Models\Setting::where('namesetting',$settingname)->get()[0]->value;
    }

    function uploadImage($request,$destination){
        $filename = $request->getClientOriginalName();
        \Intervention\Image\Facades\Image::make($request)->resize(1600,479)->save(public_path('uploads/'.$destination.$filename),50);

        return $filename;

    }
    function checkIconSize($request){
        $dimn = getimagesize($request);
        if ($dimn[0] < 30 ||$dimn[1] < 30){
            return true;
        }
    }

    function uploadThumb($request){
         \Intervention\Image\Facades\Image::make('public/uploads/service/photo/'.$request)->resize(300,null,function ($ratio){
            $ratio->aspectRatio();
        })->save(public_path('uploads/service/photo/thumb/'.$request),50);
    }

function uplodevideo($file)
{


  $name_video = $file->getClientOriginalName();
  $name = $file->getClientOriginalName();
  $realpath = $file->getRealPath();

   $file->move(public_path('uploads/videos'),$name);
  return $name;


}

}
