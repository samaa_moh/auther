<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class blogs extends Model
{
  protected $table='blogs';
  protected $fillable = [
      'title','description', 'photo',
  ];

  public function get_videos()
  {
      return $this->belongsTo(videos::class);
  }


}
