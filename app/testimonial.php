<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class testimonial extends Model
{

  protected $table='testimonials';
  protected $fillable = [
      'title','description', 'photo',
  ];

  public function get_videos()
  {
      return $this->belongsTo(videos::class);
  }



}
