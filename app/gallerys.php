<?php

namespace App;
use App\cagallery;

use Illuminate\Database\Eloquent\Model;

class  gallerys extends Model
{
  protected $table='galleries';
  protected $fillable = [
      'title','description','photo'
  ];

  public function cgallerys()
  {
       return $this->belongsTo(cagallery::class,'cgalleries_id');
  }


}
