<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;
class CarController extends Controller
{
    public function create()
    {
        return view('admin.carcreate');
    }

    public function store(Request $request)
    {
        $car = new Car();
        
        $car->carcompany = $request->get('carcompany');
        $car->model = $request->get('model');
        $car->price = $request->get('price');    
        $newcar=$request->all();   
       
        return redirect('admin/car')->with('success', 'Car has been successfully added');
    }


public function index()
{
    $cars=Car::all();
    return view('admin.carshow',compact('cars'));
}


}
