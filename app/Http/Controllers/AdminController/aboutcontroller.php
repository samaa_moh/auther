<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Facades\App\Helper\IceHelper;
use App\abouts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class aboutcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//        $this->middleware('admin:admin');
    }
    public function index()
    {
        //
        // $locale= \Illuminate\Support\Facades\App::setLocale('ar');
        $sliders = abouts::all();
        return view('admin.about.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
//        dd(auth()->user());
        // if (Auth::guard('admin')->user()->can('sliders.create')) {
            return view('admin.about.create');
        // }
        // return redirect()->back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'description' => 'required',
            'photo' => 'required',

        ]);

        $slider = new abouts();



        $slider->description = $request->description;
        $slider->image = IceHelper::uploadImage($request->file('photo'),'slider/');


        $slider->save();
        return redirect('/admin/about')->withFlashMessage('abouts Added !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        //

            $slider = abouts::find($id);
            return view('admin.about.edit',compact('slider'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request,[

            'description' => 'required'
        ]);

        $slider = abouts::find($id);


        $slider->description = $request['description'];
        if($file = $request->file('photo')){
            $slider->image = IceHelper::uploadImage($request->file('photo'),'slider/');
        }else{
            $slider->image = $slider->photo;
        }



        $slider->save();

        return redirect('/admin/about')->withFlashMessage('abouts Edited !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        // if (Auth::guard('admin')->user()->can('sliders.delete')) {
            $slider = abouts::find($id);

            $slider->delete();
            return redirect()->back()->withFlashMessage('about Deleted !!');
        // }
        // return redirect()->back();
    }
}
