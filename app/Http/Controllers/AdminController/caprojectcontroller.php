<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Facades\App\Helper\IceHelper;
use App\caprojects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class caprojectcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//        $this->middleware('admin:admin');
    }
    public function index()
    {
        //
        // $locale= \Illuminate\Support\Facades\App::setLocale('ar');
        $sliders = caprojects::all();
        return view('admin.caproject.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
//        dd(auth()->user());
        // if (Auth::guard('admin')->user()->can('sliders.create')) {

          $caprojects = caprojects::all();

            return view('admin.caproject.create',compact('caprojects'));


        // }
        // return redirect()->back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[


            'name' => 'required',

        ]);

        $slider = new caprojects();



        $slider->name = $request->name;



        $slider->save();
        return redirect('/admin/caproject')->withFlashMessage('caprojects Added !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        //

            $slider = caprojects::find($id);
            return view('admin.caproject.edit',compact('slider'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request,[

            'name' => 'required'
        ]);

        $slider = caprojects::find($id);


        $slider->name = $request['name'];




        $slider->save();

        return redirect('/admin/caproject')->withFlashMessage('caprojects Edited !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        // if (Auth::guard('admin')->user()->can('sliders.delete')) {
            $slider = caprojects::find($id);

            $slider->delete();
            return redirect()->back()->withFlashMessage('caprojects Deleted !!');
        // }
        // return redirect()->back();
    }
}
