<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Facades\App\Helper\IceHelper;
use App\cagallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class cgalleryctcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//        $this->middleware('admin:admin');
    }
    public function index()
    {
        //
        // $locale= \Illuminate\Support\Facades\App::setLocale('ar');
        $sliders = cagallery::all();
        return view('admin.cagallery.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
//        dd(auth()->user());
        // if (Auth::guard('admin')->user()->can('sliders.create')) {

          $caprojects = cagallery::all();

            return view('admin.cagallery.create',compact('caprojects'));


        // }
        // return redirect()->back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[


            'name' => 'required',

        ]);

        $slider = new cagallery();



        $slider->name = $request->name;



        $slider->save();
        return redirect('/admin/cgallery')->withFlashMessage('caprojects Added !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        //

            $slider = cagallery::find($id);
            return view('admin.cagallery.edit',compact('slider'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request,[

            'name' => 'required'
        ]);

        $slider = cagallery::find($id);


        $slider->name = $request['name'];




        $slider->save();

        return redirect('/admin/cgallery')->withFlashMessage('caprojects Edited !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        // if (Auth::guard('admin')->user()->can('sliders.delete')) {
            $slider = cagallery::find($id);

            $slider->delete();
            return redirect()->back()->withFlashMessage('cagallery Deleted !!');
        // }
        // return redirect()->back();
    }
}
