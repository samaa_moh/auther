<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Facades\App\Helper\IceHelper;
use App\features;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class featurecontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//        $this->middleware('admin:admin');
    }
    public function index()
    {
        //

        $sliders = features::all();
        return view('admin.features.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
//        dd(auth()->user());
        // if (Auth::guard('admin')->user()->can('sliders.create')) {
            return view('admin.features.create');
        // }
        // return redirect()->back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:50',

            'photo' => 'required',
            'description' => 'required',


        ]);

        $slider = new features();


        $slider->title = $request->title;

        $slider->description = $request->description;


        $slider->photo = IceHelper::uploadImage($request->file('photo'),'slider/');
        $slider->save();
        return redirect('/admin/feature')->withFlashMessage('team Added !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        //

            $slider = features::find($id);
            return view('admin.features.edit',compact('slider'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //



        $slider = features::find($id);


                $slider->title = $request->title;

                $slider->description = $request->description;



        if($file = $request->file('photo')){
            $slider->photo = IceHelper::uploadImage($request->file('photo'),'slider/');
        }else{
            $slider->photo = $slider->photo;
        }
        $slider->save();

        return redirect('/admin/feature')->withFlashMessage('features Edited !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        // if (Auth::guard('admin')->user()->can('sliders.delete')) {
            $slider = features::find($id);
            unLink(base_path().'/public/uploads/slider/'.$slider->photo);
            $slider->delete();
            return redirect()->back()->withFlashMessage('features Deleted !!');
        // }
        // return redirect()->back();
    }
}
