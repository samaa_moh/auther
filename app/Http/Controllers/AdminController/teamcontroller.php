<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Facades\App\Helper\IceHelper;
use App\team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class teamcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//        $this->middleware('admin:admin');
    }
    public function index()
    {
        //
       // $locale= \Illuminate\Support\Facades\App::setLocale('ar');
        $sliders = team::all();
        return view('admin.team.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
//        dd(auth()->user());
        // if (Auth::guard('admin')->user()->can('sliders.create')) {
            return view('admin.team.create');
        // }
        // return redirect()->back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:50',

            'photo' => 'required',
            'name' => 'required',
            'gmail' => 'required',
            'facbook' => 'required',

        ]);

        $slider = new team();


        $slider->title = $request->title;
        $slider->name = $request->name;
        $slider->gmail = $request->gmail;
        $slider->facbook = $request->facbook;


        $slider->photo = IceHelper::uploadImage($request->file('photo'),'slider/');
        $slider->save();
        return redirect('/admin/team')->withFlashMessage('team Added !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //
            $slider = team::find($id);
            return view('admin.team.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request,[
            'title' => 'required|max:50',
            'name' => 'required',
            'gmail' => 'required',
            'facbook' => 'required',
        ]);

        $slider = team::find($id);

        $slider->title = $request->title;
        $slider->name = $request->name;
        $slider->gmail = $request->gmail;
        $slider->facbook = $request->facbook;


        if($file = $request->file('photo')){
            $slider->photo = IceHelper::uploadImage($request->file('photo'),'slider/');
        }else{
            $slider->photo = $slider->photo;
        }
        $slider->save();

        return redirect('/admin/team')->withFlashMessage('team Edited !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        // if (Auth::guard('admin')->user()->can('sliders.delete')) {
            $slider = team::find($id);
          //  unLink(base_path().'/public/uploads/slider/'.$slider->photo);
            $slider->delete();
            return redirect()->back()->withFlashMessage('team Deleted !!');
        // }
        // return redirect()->back();
    }
}
