<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Facades\App\Helper\IceHelper;
use App\contacts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class contactcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//        $this->middleware('admin:admin');
    }
    public function index()
    {
        //
       // $locale= \Illuminate\Support\Facades\App::setLocale('ar');
        $sliders = contacts::all();
        return view('admin.contacts.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
//        dd(auth()->user());
        // if (Auth::guard('admin')->user()->can('sliders.create')) {
            return view('admin.contacts.create');
        // }
        // return redirect()->back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|max:50',
            'address' => 'required',
            'phone' => 'required',
            'facbook' => 'required',
        ]);

        $slider = new contacts();

        $slider->email = $request->email;
        $slider->address = $request->address;
        $slider->phone = $request->phone;
        $slider->facbook = $request->facbook;

        $slider->save();
        return redirect('/admin/contacts')->withFlashMessage('contacts Added !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        //

            $slider = contacts::find($id);
            return view('admin.contacts.edit',compact('slider'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request,[
            'email' => 'required|max:50',
            'address' => 'required',
            'phone' => 'required',
            'facbook' => 'required',
        ]);

        $slider = contacts::find($id);

        $slider->email = $request->email;
        $slider->address = $request->address;
        $slider->phone = $request->phone;
        $slider->facbook = $request->facbook;

        $slider->save();

        return redirect('/admin/contacts')->withFlashMessage('contacts Edited !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // if (Auth::guard('admin')->user()->can('sliders.delete')) {
            $slider = contacts::find($id);
            $slider->delete();
            return redirect()->back()->withFlashMessage('contacts Deleted !!');
        // }
        // return redirect()->back();
    }
}
