<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Facades\App\Helper\IceHelper;
use App\servicess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class servicescontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//        $this->middleware('admin:admin');
    }
    public function index()
    {
        //
        $locale= \Illuminate\Support\Facades\App::setLocale('ar');
        $sliders = servicess::all();
        return view('admin.services.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
//        dd(auth()->user());
        // if (Auth::guard('admin')->user()->can('sliders.create')) {
            return view('admin.services.create');
        // }
        // return redirect()->back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'required',
            'title' => 'required'

        ]);

        $slider = new servicess();



        $slider->title = $request->title;
        $slider->description = $request->description;
        $slider->photo = IceHelper::uploadImage($request->file('photo'),'slider/');

        $slider->save();
        return redirect('/admin/services')->withFlashMessage('servicess Added !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        //

            $slider = servicess::find($id);
            return view('admin.services.edit',compact('slider'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request,[

            'description' => 'required'
        ]);

        $slider = servicess::find($id);


        $slider->description = $request['description'];
        $slider->title = $request->title;



        if($file = $request->file('photo')){
            $slider->photo = IceHelper::uploadImage($request->file('photo'),'slider/');
        }else{
            $slider->photo = $slider->photo;
        }


        $slider->save();

        return redirect('/admin/services')->withFlashMessage('abouts Edited !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        // if (Auth::guard('admin')->user()->can('sliders.delete')) {
            $slider = servicess::find($id);

            $slider->delete();
            return redirect()->back()->withFlashMessage('servicess Deleted !!');
        // }
        // return redirect()->back();
    }
}
