<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Facades\App\Helper\IceHelper;
use App\slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class slidercontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//        $this->middleware('admin:admin');
    }
    public function index()
    {
        //
        // $locale= \Illuminate\Support\Facades\App::setLocale('ar');
        $sliders = slider::all();

         return view('admin.slider.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
//        dd(auth()->user());
        // if (Auth::guard('admin')->user()->can('sliders.create')) {
            return view('admin.slider.create');
        // }
        // return redirect()->back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:50',
            'description' => 'required',
            'photo' => 'required'
        ]);

        $slider = new slider();


        $slider->title = $request->title;
        $slider->description = $request->description;
        $slider->photo = IceHelper::uploadImage($request->file('photo'),'slider/');
        $slider->save();
        return redirect('/admin/slider')->withFlashMessage('Slider Added !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        //

            $slider = slider::find($id);
            return view('admin.slider.edit',compact('slider'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request,[
            'title' => 'required|max:50',
            'description' => 'required'
        ]);

        $slider = slider::find($id);

        $slider->title       = $request['title'];
        $slider->description = $request['description'];

        if($file = $request->file('photo')){
            $slider->photo = IceHelper::uploadImage($request->file('photo'),'slider/');
        }else{
            $slider->photo = $slider->photo;
        }
        $slider->save();

        return redirect('/admin/slider')->withFlashMessage('Slider Edited !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        // if (Auth::guard('admin')->user()->can('sliders.delete')) {
            $slider = slider::find($id);
            unLink(base_path().'/public/uploads/slider/'.$slider->photo);
            $slider->delete();
            return redirect()->back()->withFlashMessage('Slider Deleted !!');
        // }
        // return redirect()->back();
    }
}
