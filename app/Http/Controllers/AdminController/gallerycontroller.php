<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Facades\App\Helper\IceHelper;
use App\gallerys;
use App\cagallery;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class gallerycontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
//        $this->middleware('admin:admin');
    }
    public function index()
    {
        //

        $sliders = gallerys::all();
        return view('admin.gallerys.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
//        dd(auth()->user());
        // if (Auth::guard('admin')->user()->can('sliders.create')) {
        $caprojects = cagallery::all();
        return view('admin.gallerys.create',compact('caprojects'));


        // }
        // return redirect()->back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:50',

            'photo' => 'required',
            'caproject'=>'required',
            'description' => 'required',


        ]);

        $slider = new gallerys();


        $slider->title = $request->title;
        $slider->cgalleries_id = $request->caproject;



        $slider->description = $request->description;


        $slider->photo = IceHelper::uploadImage($request->file('photo'),'slider/');
        $slider->save();
        return redirect('/admin/gallery')->withFlashMessage('team Added !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


       $caprojects = cagallery::all();

            $slider = gallerys::find($id);

            return view('admin.gallerys.edit',compact('slider','caprojects'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //



        $slider = gallerys::find($id);


                $slider->title = $request->title;

                $slider->description = $request->description;
                    $slider->cgalleries_id = $request->caproject;



        if($file = $request->file('photo')){
            $slider->photo = IceHelper::uploadImage($request->file('photo'),'slider/');
        }else{
            $slider->photo = $slider->photo;
        }
        $slider->save();

        return redirect('/admin/gallery')->withFlashMessage('features Edited !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        // if (Auth::guard('admin')->user()->can('sliders.delete')) {
            $slider = gallerys::find($id);
            //unLink(base_path().'/public/uploads/slider/'.$slider->photo);
            $slider->delete();
            return redirect()->back()->withFlashMessage('features Deleted !!');
        // }
        // return redirect()->back();
    }
}
