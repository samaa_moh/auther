<?php

namespace App\Http\Controllers;

use App\Providers\AppServiceProvider;
use Illuminate\Http\Request;
use Auth;
 use App\publications;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // protected $test;

    public function __construct()
    {
       // $this->test = $test;

        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
 $seg = Auth::guard('web')->user()->id;
  $publications = publications::orderBy('id', 'DESC')->Where('user_id', '=', $seg)->paginate(10);

              return view('website.publication',compact('publications'));


    }
}
   