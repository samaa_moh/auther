<?php

namespace App\Http\Controllers;

use App\ourcontacts;
use Illuminate\Http\Request;

class OurcontactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $ourcontacts = ourcontacts::all();
        return view('admin.Ourcontacts.index',compact('ourcontacts'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'form-name' => ['required'],
            'form-email' => 'required',
            'form-subject' => 'required',
            'form-message' => 'required'
        ]);
        $ourcontacts = new ourcontacts();

        $ourcontacts->name  = request('form-name') ;
		$ourcontacts->email  = request('form-email') ;
        $ourcontacts->subject  = request('form-subject') ;
        $ourcontacts->message  = request('form-message') ;
        
        $ourcontacts->save();

		return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ourcontacts  $ourcontacts
     * @return \Illuminate\Http\Response
     */
    public function show(ourcontacts $ourcontacts)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ourcontacts  $ourcontacts
     * @return \Illuminate\Http\Response
     */
    public function edit(ourcontacts $ourcontacts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ourcontacts  $ourcontacts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ourcontacts $ourcontacts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ourcontacts  $ourcontacts
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $ourcontacts = ourcontacts::find($id);
        $ourcontacts->delete();
        return redirect()->back()->withFlashMessage('contact Deleted !!');

    }
}
