<?php

namespace App\Http\Controllers\websitecontroller;

use App\Http\Controllers\Controller;

use App\profiloes;
use App\findme;

use Auth;

use Redirect ;
use Validator;
use App\role;
 use  App\subjectarea;
use Illuminate\Support\Facades\Input;


use Illuminate\Http\Request;

class profile extends Controller
{
 
  public function index()
  { 
      
      
    $auth = Auth::guard('web')->user()->id;

  	$profiloes = profiloes::where('user_id', $auth)->with('get_role','get_subjectareas')->get();
  	//dd($profiloes);

   $profiloes_Roles =  profiloes::with('get_role')->get();
 //dd($profiloes_Roles);

    $Roles =  role::all();
    $subjectareas =  subjectarea::all();
    // dd($subjectareas);
   
  	$findme = findme::where('user_id', $auth)->first();
 

      return view('website.profile',compact('profiloes','findme','Roles','profiloes_Roles','subjectareas'));
 

  }

  public function edit(Request $request)
  {
  	$auth = Auth::guard('web')->user()->id;

  	 $data= profiloes::where('user_id', $auth)->update(
  	    	array(
        'title' => $request->title,
        'firstname' => $request->firstname,
        'middlename' => $request->middlename,            
        'lastname' => $request->lastname,
        'roles_id' => $request->roles,
        'subjectareas_id' => $request->subjectareas_id,
        'country' => $request->country,
        'institutionalaffiliation'=> $request->institutionalaffiliation,
        'user_id' => $auth,

       
    ));
  	 // return  $data;
  	  return Redirect::back();
  }



  public function editfindme(Request $request)
  {
  	$auth = Auth::guard('web')->user()->id;

  	 $data= findme::where('user_id', $auth)->update(
  	    	array(
        'orcid' => $request->orcid,
        'gate' => $request->gate,
        'academia' => $request->academia,
        'mendeley' => $request->mendeley,            
        'twitter' => $request->twitter,
        'facebook' => $request->facebook,
        'linkedin' => $request->linkedin,
        'user_id' => $auth,

       
    ));
  	
  	  return Redirect::back();
  }



public function uplodeimage (Request $request)
{


        // $this->validate($request,[
        //     'imgInp' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',   
        // ]);
        
        
 $seg = $request->userid;


      $validator = Validator::make($request->all(), [

           'imgInp' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',  


        ]);

    

        if ($validator->passes()) {
        //   $seg = Auth::guard('web')->user()->id;
if (Input::hasFile('imgInp'))
{

    $file = Input::file('imgInp');
    $name = $file->getClientOriginalName();
    $realpath = $file->getRealPath();

    $file->move(public_path('uplodes/userimage'),$name);

 $data= profiloes::Where('user_id', '=', $seg)->get()->first()->update(
          array(
        'image' => $name,
      ));

            return response()->json(['success'=>'Added new records.']);

        }

     }

      return response()->json(['error'=>$validator->errors()->all()]);


// $seg = Auth::guard('web')->user()->id;
// if (Input::hasFile('imgInp'))
// {

//     $file = Input::file('imgInp');
//     $name = $file->getClientOriginalName();
//     $realpath = $file->getRealPath();

//     $file->move(public_path('uplodes/userimage'),$name);

//  $data= profiloes::Where('user_id', '=', $seg)->get()->first()->update(
//           array(
//         'image' => $name,
//       ));
//  return $name;

// }



}



}
