<?php

namespace App\Http\Controllers\websitecontroller;

use App\Http\Controllers\Controller;
use App\publications;
use App\profiloes;
use App\findme;
use Auth;

use Illuminate\Http\Request;

class myprofile extends Controller
{

  public function index()
  { 
     
  	     $auth = Auth::guard('web')->user()->id;
  	     
  		$publications = publications::where('user_id', $auth)->get();
  		$findme = findme::where('user_id', $auth)->first();

  	    $profiloes = profiloes::where('user_id', $auth)->first();

      return view('website.myprofile',compact('profiloes','publications','findme'));


  }



}
