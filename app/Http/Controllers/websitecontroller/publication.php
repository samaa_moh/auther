<?php
namespace App\Http\Controllers\websitecontroller;

use App\Http\Controllers\Controller;
use Auth;

Use Redirect ;

 use App\publications;
 
use Illuminate\Http\Request;

class publication extends Controller
{

  public function index()
  {
     // $user_id = \Illuminate\Support\Facades\Auth::guard('web')->user()->id;
$seg = Auth::guard('web')->user()->id;
  $publications = publications::orderBy('id', 'DESC')->Where('user_id', '=', $seg)->paginate(10);


      return view('website.publication',compact('publications'));

  }


  public  function  edit($puid)
  {

if (publications::where('puid', '=', $puid)->count() == 0) {
return abort(404);
}else{
  $publications = publications::where('puid', $puid)->first();


      return view('website.authors',compact('publications'));
}

  }

  public  function  update (Request $request,$puid)
  {
 


//   $data=    publications::Where('puid', '=', $puid)->get()->first();
// dd($data);
  	 $data= publications::Where('puid', '=', $puid)->get()->first()->update(
  	    	array(
        'plaintitle' => $request->plain,
        'about' => $request->about,
        'important' => $request->important,            
        'Perspectives' => $request->perspectives,
       
    ));
  	 // return  $data;
  	  return Redirect::back();
  }

}
