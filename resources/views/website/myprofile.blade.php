<!DOCTYPE html> 
<html lang="en">
    <head>
        <meta charset="utf-9" />

        <title>My Profile</title>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" />
       
       @include('website.includes.css')

    </head>
    <body> 

        <!-----Header------>
@include('website.includes.header')



        <!---Section-------->
        <section class="w-100 py-5 main_screen my_profile_maine">

            <div class="container clearfix px-0 px-md-3">
                <div class="de_full de_view">
                    
                    
                    <!-------Script Code !!!------------>
                    <div  class="col-12 col-md-12 col-lg-6 col-xl-4 g_one"></div>
                    <!-------Script Code !!!------------>
                    
                    
                    <div class="col-12 pb-5 px-0 px-md-3">
                        <div class="inp_pub p-3  row mx-0">
                            <div class="col-12 pt-2 col-lg-3">
                                <div class="user-pro">
                                    <img src="{{Request::root()}}/public/uplodes/userimage/{{$profiloes->image}}" />
                                </div>
                            </div>   

                            <div class="col-12 col-lg-9 pt-4 pt-lg-2 d-flex align-items-center">
                                <div class="head_profile col-12 p-0">
                                    <h2 class="h2 pb-3 m-0"><span class="sup_title">DR :</span> {{Auth::guard('web')->user()->name}}</h2>
                                    <div class="row mx-0 bo_top pt-3">
                                        <ul class="list_ul col-12 col-lg-4">
                                            <li>{{$profiloes->institutionalaffiliation}}</li>
                                            <li>Lecturer, Analytics</li>
                                            <li>{{$profiloes->country}}</li>
                                        </ul>

                                        <ul class="list_ul list_ul_a  col-12 col-lg-8">
        
                                            <li><a class="a_list tw_a" href="{{$findme->twitter}}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                            <li><a class="a_list fc_a" href="{{$findme->facebook}}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a class="a_list in_a" href="{{$findme->linkedin}}" target="_blank"><i class="fab fa-linkedin-in" target="_blank"></i></a></li>
                                            <li><a class="a_list ube_a" href="{{$findme->youtube}}" target="_blank"><i class="fab fa-youtube"></i></a></li>
                                   
                                        </ul>                                            
                                    </div>

                                </div>
                            </div>

                        <!--     <div class="col-12 bo_top my-5 py-4">
                                <h4 class="h4 color_app_text_1"><i class="fas fa-book"></i> My co-authors include</h4>
                                <div class="row pt-4">
                                    <div class="col-12 col-md-auto py-2">
                                        <a href="#" class="for_authors w-100" >
                                            <div class="photo_authors">
                                                <img src="img/avtar.png" />
                                            </div>
                                            Some Name
                                        </a>
                                    </div>
                                    <div class="col-12 col-md-auto py-2">
                                            <a href="#" class="for_authors w-100" >
                                                <div class="photo_authors">
                                                    <img src="img/avtar.png" />
                                                </div>
                                                Some Name
                                            </a>
                                        </div>

                                        <div class="col-12 col-md-auto py-2">
                                                <a href="#" class="for_authors w-100" >
                                                    <div class="photo_authors">
                                                        <img src="img/avtar.png" />
                                                    </div>
                                                    Some Name
                                                </a>
                                            </div>    
                                </div>
                            </div>

 -->

                            <div class="col-12 bo_top  mt-5 py-4">
                                <h4 class="h4 color_app_text_1"><i class="fas fa-archive"></i> My Publications</h4>
                                <div class="row py-5 ">
                                     @foreach($publications as $publication)    
                                    <article class="arti_profile col-12 col-md-auto">
                                        <a href="{{$publication->link}}" class="coll_for_u row mx-0">
                                            <div class="col-4 d-flex justify-content-center align-items-center">

                                                <img class="img-fluid" src="{{ asset('assets/website/img/item-2.png') }}" />
                                            </div>
                                            <div class="col-8 coll_con ">
                                                <h6 class="h6 mb-1">{{$publication->title}}</h6>
                                                <p class="small ">{{$publication->publisher}}</p> 
                                                <p class="small m-0">{{$publication->date}}</p>
                
                                            </div>
                                        </a>

                                    </article>
                                    
                        @endforeach
                                               
                                                                        
                                </div>




                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>


 @include('website.includes.footer')
        <!-- END FOOTER -->
        <!-- End footer -->

@include('website.includes.js')
    </body> 
</html>