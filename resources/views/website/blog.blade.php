﻿<!DOCTYPE html>
<html lang="en">
	
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title> almasria4ceiling / blogs </title>
		@include('website.includes.css')
	</head>

	<body>
		<!--loader-->
		<div id="preloader">
			<div class="sk-circle">
				<div class="sk-circle1 sk-child"></div>
				<div class="sk-circle2 sk-child"></div>
				<div class="sk-circle3 sk-child"></div>
				<div class="sk-circle4 sk-child"></div>
				<div class="sk-circle5 sk-child"></div>
				<div class="sk-circle6 sk-child"></div>
				<div class="sk-circle7 sk-child"></div>
				<div class="sk-circle8 sk-child"></div>
				<div class="sk-circle9 sk-child"></div>
				<div class="sk-circle10 sk-child"></div>
				<div class="sk-circle11 sk-child"></div>
				<div class="sk-circle12 sk-child"></div>
			</div>
		</div>

		<!--loader-->
		<!-- HEADER -->
		<!--Start header area-->
		<!--Header Section Start Here
		==================================-->
		@include('website.includes.header')
		<!--Header End Here-->
		<!--End mainmenu area-->
		<!-- END HEADER -->
 
		<section class="inner-intro bg-img light-color overlay-before parallax-background">
			<div class="container">
				<div class="row title">
					<div class="title_row">
						<h1 data-title="Blog"><span>Blog</span></h1>
					<div class="page-breadcrumb">
						<a>Home</a>/ <span>Blog</span>
					</div>
					</div>
				</div>
			</div>
		</section>
 
 
		<!-- Blog Post Section -->
		<section class="padding pt-xs-40">
			<div class="container">

				<div class="row"> 
					<!-- Post Item -->
					<div class="col-lg-9">

						<div class="row">
							
							@foreach($blogs as $blog)
								<div class="col-md-12 col-lg-12 blog-post-hr">
									<div class="blog-post mb-45">
								
										<div class="blog-post mb-45">
											<div class="post-media"> 
												<img src="{{Request::root()}}/public/uploads/slider/{{$blog->photo}}" alt="" />
											</div>

											<div class="post-meta"> 
												<span>by <a href="javascript:avoid(0);">Admin</a>,</span>
												<span> <a href="javascript:avoid(0);"><i class="fa fa-comment-o"></i> created at {{$blog->created_at}}</a>,</span>
												<span> <a href="javascript:avoid(0);"><i class="fa fa-heart-o"></i> updated at {{$blog->updated_at}}</a>,</span>
											</div>

											<div class="post-header">
												<h2><a href="#">{{$blog->title}}</a></h2>
											</div>

											<div class="post-entry">		
												<p> {{$blog->description}} </p>
											</div>

										</div>					

									</div>
								</div>
							@endforeach
							
							<!-- Pagination Nav -->
							<div class="pagination-nav text-left mt-60 mtb-xs-30">
								<ul>
									<li> <a href="javascript:avoid(0);"><i class="fa fa-angle-left"></i></a> </li>
									<li class="active"> <a href="javascript:avoid(0);">1</a> </li>
									<li> <a href="javascript:avoid(0);">2</a> </li>
									<li> <a href="javascript:avoid(0);">3</a> </li>
									<li> <a href="javascript:avoid(0);"><i class="fa fa-angle-right"></i></a> </li>
								</ul>
							</div>
							<!-- End Pagination Nav -->    
							
						</div>
						<!-- End Post Item --> 
					</div>
				</div>

			</div>
		</section>
		<!-- End Blog Post Section -->
   

		<!-- footer -->
		<!-- FOOTER -->
		@include('website.includes.footer')
		<!-- END FOOTER -->
		<!-- End footer -->

		@include('website.includes.js')

	</body>
</html>