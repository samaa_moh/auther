  <header class="w-100">
            <nav class="container-fluid py-1">
                <div class="row">
                    <div class="col-8 col-md-4 col-lg-3">
                        
                        <!--___Icon For Aside_____-->
                        <div class="gulp_ico pr-3 d-inline-block d-flex justify-content-center align-items-center float-left">
                            <span class="ico_bar" onclick="toAside('e_aside', 'g_one')" ><i class="fas fa-bars"></i></span>
                        </div>
                        
                         
                        <!--______Ierek Logo_______ -->
                        <a class="brand_global" href="{{url('/')}}">
                            
                            <img class="global_brand" src="{{ asset('assets/website/img/IerekLogo.png') }}">
                        </a>
                    </div>

                    <!--______Search Divition________-->
                    <div class="col-md-6 colo-s4">
                        <div class="main_flex">
                            <div class="input-group">
                                <div class="input-group-btn search-panel">
                                    <button type="button" class="btn btn-default clear_foucs dropdown-toggle" data-toggle="dropdown">
                                        <span id="search_concept">Filter by</span> <span class="caret clear_foucs"> </span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#contains">all</a></li>
                                        <li><a href="#its_equal">pdf</a></li>
                                        <li><a href="#greather_than">video</a></li>
                                    </ul>
                                </div>

                                <!--_____input Search______-->
                                <input type="search" class="form-control clear_foucs" name="global_search" placeholder="Search term..." />
                                <span class="input-group-btn">
                                    <button class="btn btn-default clear_foucs" type="button"><i class="fas fa-search"></i></button>
                                </span>
                            </div>
                        </div>

                    </div>



                                 @guest
                                      <!--__________Login & Sign-up Switch_________ -->
                    <div class="main_flex db-ew float-right mx-3">
                            <div class="account_new row mx-0">
                                <div class="dd_new" onclick="toList('line_1')"><i class="fas fa-sign-in-alt"></i> Sign in</div>
                                <ul id="line_1">
                                    <!-- ___For_loging____ -->
                                    <li><a href="{{url('login')}}">Login</a></li>
                                    
                                    <!--_______Sign_Up________-->
                                    <li><a href="{{url('register')}}">SignUp</a></li>





                                </ul>
                            </div>
                    </div>

                        @else

                                            <div class="main_flex db-ew float-right mx-3">
                            <div class="account_new pro_nav row mx-0">
                                <div class="dd_new" onclick="toList('line_2')">
                                    <span class="hello-user">
                                        
                                        <img src="{{asset('assets/website/img/user.png') }}" />
                                    </span>
                                  {{ Auth::user()->name }}
                                    <i class="fas fa-sort-down"></i>
                                </div>
                                <ul id="line_2">

                                    <li><a href="{{url('my/sittings')}}"><i class="fas fa-user-tie"></i> My Profile</a></li>
                                    <li><a href="{{url('my/profile')}}"><i class="fas fa-cogs"></i> Sittings</a></li>

                                            <li><a href="{{url('home/user/logout')}}"><i class="fas fa-sign-out-alt"></i> Logout</a></li>
                                </ul>

                            </div>
                    </div>

                         
           
                        @endguest

 

                     </div>
            </nav>

            <!--________Aside_________ -->
            <aside class="aside_cr pb-5 dir_ar" id="e_aside">

                <!-- _______ Writnig_ Divition_______ -->
                <div class="head-main dir_en">
                    <h5 class="h5 title_list" id="b_head" onclick="toList('b_list', 'b_head', 'b_icons')">
                        <span class="span_icon"><i class="fas fa-marker"></i></span>
                         Writing your paper
                        <span class="ico_right" id="b_icons"><i class="fas fa-angle-right"></i></span>
                    </h5>
                    <ul class="as_list" id="b_list">
                        <li> <a href="./writing.html">ESSD</a></li>
                        <li> <a href="./writing.html">Arechive</a></li>
                        <li> <a href="./writing.html">Resourceedings</a></li>
                        <li> <a href="./writing.html">BAHETH</a></li>
                        <li> <a href="./writing.html">ASTI Book Series</a></li>
                    </ul>
                </div>

                <!-- _________Submission____________ -->
                <div class="head-main dir_en">
                    
                        <h5 class="h5 title_list" id="b_head_1" onclick="toList('b_list_1', 'b_head_1', 'b_icons_1')">
                            <span class="span_icon"><i class="fas fa-share-alt"></i></span> 
                            Making your submission
                            <span class="ico_right" id="b_icons_1"><i class="fas fa-angle-right"></i></span>
                        </h5>
                        <ul class="as_list" id="b_list_1">
                            
                            <li> <a href="{{url('publication')}}">Publications</a></li>
                        </ul>
                </div>


                <!-- _____Request AStyle Link_______ -->
                <a href="#" class="h5 dir_en title_list d-block my-3 flow_aside">
                    <span class="span_icon"><i class="fas fa-broom"></i></span> 
                    Request Astyle
                    <span class="ico_right ro_ico"><i class="fas fa-angle-right"></i></span>
                </a>
                

                <!-- _____FAQ Link_______ -->
                <a href="#" class="h5 dir_en title_list d-block my-3 flow_aside">
                    <span class="span_icon"><i class="fas fa-question-circle"></i></span> 
                    FAQ
                    <span class="ico_right ro_ico"><i class="fas fa-angle-right"></i></span>
                </a>
                

                <!-- _____BLOG Link_______ -->
                <a href="#" class="h5 dir_en title_list d-block my-3 flow_aside">
                    <span class="span_icon"><i class="fas fa-coffee"></i></span> 
                    Blog
                    <span class="ico_right ro_ico"><i class="fas fa-angle-right"></i></span>
                </a>


                <!-- _____Slider Adevrtisement Divition _______ -->
                <div class="ew_line dir_en px-2 py-3">

                    <div class="owl-carousel owl-theme">
                        <div> <a class="slide_image" href="#"><img  src="img/journalThumbnail_en_US.png" /></a> </div>
                        <div> <a class="slide_image" href="#"><img  src="img/journalThumbnail_en_US (1).png" /></a> </div>
                        <div> <a class="slide_image" href="#"><img  src="img/journalThumbnail_en_US (2).png" /></a> </div>
                        <div> <a class="slide_image" href="#"><img  src="img/journalThumbnail_en_US.png" /></a> </div>
                        <div> <a class="slide_image" href="#"><img  src="img/journalThumbnail_en_US (1).png" /></a> </div>
                        <div> <a class="slide_image" href="#"><img  src="img/journalThumbnail_en_US (2).png" /></a> </div>
                    </div>
                </div>

            </aside>
        </header>
