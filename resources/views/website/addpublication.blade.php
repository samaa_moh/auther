﻿


<!DOCTYPE html> 
<html lang="en">
    <head>
        <meta charset="utf-9" />

        <title>Publications</title>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
     @include('website.includes.css')

    </head>
    <body>

 
         <!-- _______Header___________ -->
@include('website.includes.header')

        <!---Section-------->
        <section class="w-100 py-5 main_screen publication_main">

            <div class="container clearfix px-0 px-md-3">
                <div class="de_full de_view">
                    
                    
                    <!-------Script Code !!!------------>
                    <div  class="col-12 col-md-12 col-lg-6 col-xl-4 g_one"></div>
                    <!-------Script Code !!!------------>
                    
                    
                        <div class="w-100 inp_pub py-3 row mx-0">
                            <div class="col-12 h2 color_app_text_1 text-center">Add publication</div>
                            <p class="col-12 m-0 pb-3 small bo_bottom color_app_text_1 text-center">There are a number of ways to add your publications to.</p>
                        
                            <form class="for_publications col-12 col-md-6 mx-auto col-xl-4 col-lg-6 my-3  text-center">

                                <h4 class="a_h_n head_pub color_app_text_1">Enter the DOI for your publication</h4>



                                <div class="a_b_n col-12">
                                        <input id="id_allow" type="text" name="id" value="{{\Illuminate\Support\Facades\Auth::guard('web')->user()->id}}" class="form-control d-none" required />

                                        <input id="id_doi_src" type="text" class="form-control" placeholder="DOI, e.g. 10.1234/2156152" required />
                                        <div id="ck_doi" class="btn sub_me buo_a"  >
                                            <div class="un_sub go_submit d-none"></div>

                                            <div class="w-100" id="in_submit">
                                               Find my publication
                               
                                                 <div class="spinner-border spinner-border-sm d-none" role="status">
                                                  <span class="sr-only">Loading...</span>
                                                </div>                           

                                            </div>

                                        </div>
                                    </div>    
                                  

                                 
                                <!-- ____Ajax _Response_______  -->
                                <div id="ajax_id" class=" w-100 ne_ajax">
                                    <div class="color_app_text_1 small text-left ">
                                        <div class="w-100 bo_top  ne_ajax mt-4 pt-4" id="vew_line">
                                            <a id="a_href"  target="_blank"></a>
                                            <p id="map_lan" class="m-0"></p> 
                                            <p class="m-0" id="de_journal">
                                                
                                            </p>
                                            <a href="#" id="a_go_3" class="btn btn-save py-0 px-3 mt-2">Go</a>
                                        </div>

                                        <P id="nt_finde" class="d-none text-danger mt-3"></P>
                                    </div>
                                </div>
                            </form>

                            <form class="for_publications col-12 col-md-6 mx-auto col-xl-4 col-lg-6 my-3  text-center">

                                    <h4 class="a_h_n head_pub color_app_text_1">Search for your publication</h4>

                                    <div class="a_b_n col-12">
                                            <input type="text" class=" form-control" placeholder="Title, author, DOI, keywords, etc." /> 
                                            <button type="submit" class="btn buo_a">Search for my publication</button>
                                        </div>       
                                </form>


                                <form class="for_publications col-12 col-md-6 mx-auto col-xl-4 col-lg-6 my-3 text-center">

                                        <h4 class="a_h_n head_pub color_app_text_1">Import your publications from ORCID</h4>
                                        
                                        <div class="a_b_n col-12">
                                            <p class="small m-0">Connect your ORCID and we will automatically import your publications and update periodically. For more information see our <a href="#">FAQs page</a></p>
   
                                            <button type="submit" class="btn buo_a">Create or connect your ORCID</button>
                                        </div>                                       
            
                                    </form>    

                                <div class="w-100 text-center bo_top mt-3">

                                    <a class="btn a_global buo_a" href="publication"><i class="fas fa-angle-left"></i> Back To publications</a>
                                </div>
                        </div>
                    
                </div>

            </div>
        </section>



 @include('website.includes.footer')
        <!-- END FOOTER -->
        <!-- End footer -->

@include('website.includes.js')
    </body> 
</html>