﻿<!DOCTYPE html>
<html lang="en">

<!-- you  from theembazaar.com/demo/themejio/spaceradius/project.html by you  Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 08:16:36 GMT -->
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title> almasria4ceiling / Projects </title>
		@include('website.includes.css')
	</head>
	<body>
		<!--loader-->
		<div id="preloader">
			<div class="sk-circle">
				<div class="sk-circle1 sk-child"></div>
				<div class="sk-circle2 sk-child"></div>
				<div class="sk-circle3 sk-child"></div>
				<div class="sk-circle4 sk-child"></div>
				<div class="sk-circle5 sk-child"></div>
				<div class="sk-circle6 sk-child"></div>
				<div class="sk-circle7 sk-child"></div>
				<div class="sk-circle8 sk-child"></div>
				<div class="sk-circle9 sk-child"></div>
				<div class="sk-circle10 sk-child"></div>
				<div class="sk-circle11 sk-child"></div>
				<div class="sk-circle12 sk-child"></div>
			</div>
		</div>

		<!--loader-->
		<!-- HEADER -->
		<!--Start header area-->
		<!--Header Section Start Here
		==================================-->
		@include('website.includes.header')
		<!--Header End Here-->
		<!--End mainmenu area-->
		<!-- END HEADER -->
		<!-- CONTENT -->
		<!-- Intro Section -->
		<section class="inner-intro bg-img light-color overlay-before parallax-background">
			<div class="container">
				<div class="row title">
					<div class="title_row">
						<h1 data-title="Project"><span>Project</span></h1>
						<div class="page-breadcrumb">
							<a>Home</a>/ <span>Project</span>
						</div>

					</div>

				</div>
			</div>
		</section>
		<!-- End Intro Section -->
		<!-- Work Section -->
		<section id="work" class="padding ptb-xs-40">
			<div class="container">
				<div class="row text-center">
					<div class="col-lg-6 offset-lg-3 mb-30">
						<div class="heading-box">
							<h2><span>Our </span> Project</h2>
							<p>
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
							</p>
						</div>

					</div>
				</div>
				<!-- work Filter -->
				<div class="row">
					<div class="col-lg-6 offset-lg-3">
						<ul class="container-filter categories-filter">
							<li>
								<a class="categories active" data-filter="*">All</a>
							</li>

							 @foreach($caprojects as $caproject)
							<li>
								<a class="categories" data-filter=".{{$caproject->id}}" >{{$caproject->name}}</a>
							</li>
							 @endforeach
						</ul>

					</div>
				</div>
				<!-- End work Filter -->
				<div class="row container-grid nf-col-3">


 @foreach($projects as $project)


 <div class="nf-item {{ $project->caprojects()->first()->id}} spacing">

		<div class="item-box">
			<a> <img alt="1" src="{{Request::root()}}/public/uploads/slider/{{$project->photo}}" class="item-container"> </a>
			<div class="link-zoom">
				<a href="{{Request::root()}}/public/uploads/slider/{{$project->photo}}" class="project_links"> <i class="fa fa-link"> </i> </a>
				<a href="{{Request::root()}}/public/uploads/slider/{{$project->photo}}" class="fancylight" data-fancybox-group="light" > <i class="fa fa-search-plus"></i> </a>
			</div>
			<div class="gallery-heading">
				<h4><a href="#">{{ $project->title}}</a></h4>
				<p>
				{{ $project->description}}
				</p>
			</div>
		</div>

	</div>

 @endforeach




				</div>

			</div>
		</section>
		<!-- End Work Section -->
		<!--End Contact-->
		<!-- FOOTER -->
		@include('website.includes.footer')
		<!-- END FOOTER -->

		@include('website.includes.js')
		<script src="http://theembazaar.com/demo/themejio/spaceradius/assets/js/jquery.mousewheel-3.0.6.pack.js" type="text/javascript"></script>
		<script src="http://theembazaar.com/demo/themejio/spaceradius/assets/js/jquery.fancybox.pack.js" type="text/javascript"></script>

	</body>

<!-- you  from theembazaar.com/demo/themejio/spaceradius/project.html by you  Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 08:16:36 GMT -->
</html>
