﻿<!DOCTYPE html>
<html lang="en">
	
<!-- 00 from theembazaar.com/demo/themejio/spaceradius/team.html by you  Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 10:24:18 GMT -->
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title> almasria4ceiling / Team </title>
		@include('website.includes.css')
	</head>
	<body>
		<!--loader-->
		<div id="preloader">
			<div class="sk-circle">
				<div class="sk-circle1 sk-child"></div>
				<div class="sk-circle2 sk-child"></div>
				<div class="sk-circle3 sk-child"></div>
				<div class="sk-circle4 sk-child"></div>
				<div class="sk-circle5 sk-child"></div>
				<div class="sk-circle6 sk-child"></div>
				<div class="sk-circle7 sk-child"></div>
				<div class="sk-circle8 sk-child"></div>
				<div class="sk-circle9 sk-child"></div>
				<div class="sk-circle10 sk-child"></div>
				<div class="sk-circle11 sk-child"></div>
				<div class="sk-circle12 sk-child"></div>
			</div>
		</div>

		<!--loader-->
		<!-- HEADER -->
		<!--Start header area-->
		<!--Header Section Start Here
		==================================-->
		@include('website.includes.header')
		<!--Header End Here-->
		<!--End mainmenu area-->
		<!-- END HEADER -->
			<!-- CONTENT -->
			<!-- Intro Section -->
			<section class="inner-intro bg-img light-color overlay-before parallax-background">
			<div class="container">
				<div class="row title">
					<div class="title_row">
						<h1 data-title="Team"><span>Team</span></h1>
						<div class="page-breadcrumb">
							<a>Home</a>/ <span>Team</span>
						</div>

					</div>

				</div>
			</div>
		</section>
			<!-- End Intro Section -->
			<div class=" padding ptb-xs-60">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<!--Team Section-->
							<section id="team" class="team-section">

								<div class="container">

									<div class="row text-center pb-30">
										<div class="col-sm-12">
											<div class="heading-box ">
												<h2><span>Our</span> Team</h2>
												<span class="b-line"></span>
											</div>
										</div>
									</div>
									
									<div class="row mt-80 text-center">

										@foreach($team as $member)
											<div class="col-sm-3 mb-xs-30">
												<div class="box-hover img-scale">
													<figure>
														<img src="{{Request::root()}}/public/uploads/slider/{{$member->photo}}" alt="">
													</figure>
													<div class="team-block">
														<strong>{{$member->name}}</strong>
														<span>{{$member->title}}</span>
														<hr class="small-divider border-white">
														<ul class="social-icons">
															<li>
																<a href="{{$member->facbook}}"><i class="fa fa-facebook" aria-hidden="true"></i></a>
															</li>
															<li>
																<a href="{{$member->gmail}}"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
															</li>
														</ul>
													</div>
												</div>
											</div>
										@endforeach
									</div>
									
								</div>

							</section>
							<!--Team Section End-->
						</div>
					</div>
				</div>
			</div>
			<!--End Contact-->
			<!-- FOOTER -->
		@include('website.includes.footer')
		<!-- END FOOTER -->

		@include('website.includes.js')

	</body>

<!-- 00 from theembazaar.com/demo/themejio/spaceradius/team.html by you  Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 10:24:18 GMT -->
</html>
