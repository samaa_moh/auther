﻿<!DOCTYPE html>
<html lang="en">
	
<!-- 00 from theembazaar.com/demo/themejio/spaceradius/career.html by you  Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 10:23:42 GMT -->
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title> almasria4ceiling / Career </title>
		<link href="{{ asset('assets/website/css/font-awesome.css') }}" rel="stylesheet" type="text/css">
		@include('website.includes.css')

	</head>
	<body>
		<!--loader-->
		<div id="preloader">
			<div class="sk-circle">
				<div class="sk-circle1 sk-child"></div>
				<div class="sk-circle2 sk-child"></div>
				<div class="sk-circle3 sk-child"></div>
				<div class="sk-circle4 sk-child"></div>
				<div class="sk-circle5 sk-child"></div>
				<div class="sk-circle6 sk-child"></div>
				<div class="sk-circle7 sk-child"></div>
				<div class="sk-circle8 sk-child"></div>
				<div class="sk-circle9 sk-child"></div>
				<div class="sk-circle10 sk-child"></div>
				<div class="sk-circle11 sk-child"></div>
				<div class="sk-circle12 sk-child"></div>
			</div>
		</div>

		<!--loader-->
		<!-- HEADER -->
		<!--Start header area-->
		<!--Header Section Start Here
		==================================-->
		@include('website.includes.header')
		<!--Header End Here-->
		<!--End mainmenu area-->
		<!-- END HEADER -->

		<!-- Intro Section -->
		<section class="inner-intro bg-img light-color overlay-before parallax-background">
			<div class="container">
				<div class="row title">
					<div class="title_row">
						<h1 data-title="Career"><span>Career</span></h1>
						<div class="page-breadcrumb">
							<a>Home</a>/ <span>Career</span>
						</div>

					</div>

				</div>
			</div>
		</section>
		<!-- Intro Section End-->

		<!-- CONTENT -->

		<section class="padding pt-xs-40">
			<div class="container">

				<div class="row">
					<div class="col-md-9 col-lg-9">
						<div class="tab-content">

							<div class="row">

								<div class="col-md-12 col-lg-12">
								
									<img src="{{ asset('assets/website/images/people.jpg') }}" alt="">
								</div>

								<div class="col-md-12 col-lg-12">
									<h2 class="pt-30">Open Positions</h2>
									<p>
										Lorem ipsum dolor sit amet consectetur adipisicing elit eiusmod tempor incididunt ut labore et dolore magna aliqua. Enim minim veniam quis nostrud exercitation ullamco laboris nisi aliquip exea commodo ipsum dolor sit amet consectetur adipisicing elit eiusmod tempor incididunt ut labore et dolore magna aliqua.
										laboris nisi aliquip exea commodo ipsum dolor sit amet consectetur adipisicing elit eiusmod tempor incididunt ut labore et dolore magna aliqua.
									</p>

								</div>

							</div>

						</div>

						<!-- About Section -->
						<div class="anim-section creer_page mt-30">
							<div id="accordion" role="tablist" aria-multiselectable="true">
								
								@foreach($careers as $career)
								<div class="card">
									<div class="card-header" role="tab" id="headingTwo">
										<h5 class="mb-0 panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collape{{$career->id}}" 
										aria-expanded="false" aria-controls="collape{{$career->id}}"> {{$career->title}}
										<i class="fa fa-plus collape-plus"></i> </a></h5>
									</div>
									<div id="collape{{$career->id}}" class="collapse bg-custom" role="tabpanel" aria-labelledby="headingTwo">
										<div class="card-block">
											<p>
												{{$career->description}}
											</p>
										</div>
									</div>
								</div>
								@endforeach

							</div>
						</div>
						<!-- About Section End-->

					</div>

				</div>
			</div>
		</section>

		<!-- FOOTER -->
		@include('website.includes.footer')
		<!-- END FOOTER -->
		<script src="{{ asset('assets/website/js/jquery.min.js') }}" type="text/javascript"></script>
		@include('website.includes.js')

	</body>

<!-- 00 from theembazaar.com/demo/themejio/spaceradius/career.html by you  Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 10:23:42 GMT -->
</html>
