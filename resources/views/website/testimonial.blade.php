﻿<!DOCTYPE html>
<html lang="en">
	
<!-- 00 from theembazaar.com/demo/themejio/spaceradius/testimonial.html by you  Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 10:24:48 GMT -->
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title> almasria4ceiling </title>
		@include('website.includes.css')
	</head>
	<body>
		<!--loader-->
		<div id="preloader">
			<div class="sk-circle">
				<div class="sk-circle1 sk-child"></div>
				<div class="sk-circle2 sk-child"></div>
				<div class="sk-circle3 sk-child"></div>
				<div class="sk-circle4 sk-child"></div>
				<div class="sk-circle5 sk-child"></div>
				<div class="sk-circle6 sk-child"></div>
				<div class="sk-circle7 sk-child"></div>
				<div class="sk-circle8 sk-child"></div>
				<div class="sk-circle9 sk-child"></div>
				<div class="sk-circle10 sk-child"></div>
				<div class="sk-circle11 sk-child"></div>
				<div class="sk-circle12 sk-child"></div>
			</div>
		</div>

		<!--loader-->
		<!-- HEADER -->
		<!--Start header area-->
		<!--Header Section Start Here
		==================================-->
		@include('website.includes.header')
		<!--Header End Here-->
		<!--End mainmenu area-->
		<!-- END HEADER -->
			<!-- CONTENT -->
			
			
			<!-- Intro Section -->
 <section class="inner-intro bg-img light-color overlay-before parallax-background">
    <div class="container">
      <div class="row title">
      	<div class="title_row">
      		<h1 data-title="Testimonial"><span>Testimonial</span></h1>
      		<div class="page-breadcrumb">
							<a>Home</a>/ <span>Testimonial</span>
						</div>
      		
      	</div>
        
      </div>
    </div>
  </section>
 <!-- Intro Section End-->
			
			
			<!-- Testimonial section -->
			<div class="padding pt-xs-40">
				<div class="container">
					<div class="row ">
						<div class="col-md-12 col-lg-12">
							<div class="heading-box pb-30">
								<h2><span>Our</span> Testimonial</h2>
								<span class="b-line l-left"></span>
							</div>
						</div>
					</div>
					<div class="row mt-60">
						<div class="col-md-6 col-lg-6">
							<div class="about-block gray-bg   padding-40 clearfix">
								<div class="client-avtar">
									<img src="http://theembazaar.com/demo/themejio/spaceradius/assets/images/user2-160x160.jpg" class="img-responsive" alt="Responsive image">
								</div>
								<div class="text-box">
									<div class="text-content">
										<p>
											Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..
											Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget
										</p>
									</div>
									<div class="box-title">
										<h4 class="member_say"><span>By</span> Gary jack</h4>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-6">
							<div class="about-block dark-gray-bg  padding-40 clearfix">
								<div class="client-avtar left-pos">
									<img src="http://theembazaar.com/demo/themejio/spaceradius/assets/images/user2-160x160.jpg" class="img-responsive" alt="Responsive image">
								</div>
								<div class="text-box">
									<div class="text-content">
										<p>
											Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..
											Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget
										</p>
									</div>
									<div class="box-title">
										<h4 class="member_say"><span>By</span> Gary jack</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row mt-80">
						<div class="col-md-6 col-lg-6">
							<div class="about-block dark-gray-bg   padding-40 clearfix">
								<div class="client-avtar">
									<img src="http://theembazaar.com/demo/themejio/spaceradius/assets/images/user2-160x160.jpg" class="img-responsive" alt="Responsive image">
								</div>
								<div class="text-box">
									<div class="text-content">
										<p>
											Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..
											Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget
										</p>
									</div>
									<div class="box-title">
										<h4 class="member_say"><span>By</span> Gary jack</h4>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-6">
							<div class="about-block gray-bg  padding-40 clearfix">
								<div class="client-avtar left-pos">
									<img src="http://theembazaar.com/demo/themejio/spaceradius/assets/images/user2-160x160.jpg" class="img-responsive" alt="Responsive image">
								</div>
								<div class="text-box">
									<div class="text-content">
										<p>
											Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..
											Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget
										</p>
									</div>
									<div class="box-title">
										<h4 class="member_say"><span>By</span> Gary jack</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row mt-80">
						<div class="col-md-6 col-lg-6">
							<div class="about-block gray-bg   padding-40 clearfix">
								<div class="client-avtar">
									<img src="http://theembazaar.com/demo/themejio/spaceradius/assets/images/user2-160x160.jpg" class="img-responsive" alt="Responsive image">
								</div>
								<div class="text-box">
									<div class="text-content">
										<p>
											Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..
											Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget
										</p>
									</div>
									<div class="box-title">
										<h4 class="member_say"><span>By</span> Gary jack</h4>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-6">
							<div class="about-block dark-gray-bg  padding-40 clearfix">
								<div class="client-avtar left-pos">
									<img src="http://theembazaar.com/demo/themejio/spaceradius/assets/images/user2-160x160.jpg" class="img-responsive" alt="Responsive image">
								</div>
								<div class="text-box">
									<div class="text-content">
										<p>
											Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget..
											Aenean suscipit eget mi act fermentum phasellus vulputate turpis tincidunt. Aenean suscipit eget
										</p>
									</div>
									<div class="box-title">
										<h4 class="member_say"><span>By</span> Gary jack</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Testimonial section end -->
			<!--End Contact-->
				<!-- FOOTER -->
		@include('website.includes.footer')
		<!-- END FOOTER -->

		@include('website.includes.js')

	</body>

<!-- 00 from theembazaar.com/demo/themejio/spaceradius/testimonial.html by you  Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 10:24:48 GMT -->
</html>
