﻿<!DOCTYPE html>
<html lang="en">

<!-- you  from theembazaar.com/demo/themejio/spaceradius/portfolio-grid.html by you  Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 08:21:25 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
       	<title> almasria4ceiling / Gallery </title>

	   	@include('website.includes.css')

 	</head>

<body>
<!--loader-->
<div id="preloader">
  <div class="sk-circle">
    <div class="sk-circle1 sk-child"></div>
    <div class="sk-circle2 sk-child"></div>
    <div class="sk-circle3 sk-child"></div>
    <div class="sk-circle4 sk-child"></div>
    <div class="sk-circle5 sk-child"></div>
    <div class="sk-circle6 sk-child"></div>
    <div class="sk-circle7 sk-child"></div>
    <div class="sk-circle8 sk-child"></div>
    <div class="sk-circle9 sk-child"></div>
    <div class="sk-circle10 sk-child"></div>
    <div class="sk-circle11 sk-child"></div>
    <div class="sk-circle12 sk-child"></div>
  </div>
</div>
<!--loader-->
<!-- Site Wraper -->
<div class="wrapper">
  <!-- HEADER -->
  <!--Start header area-->
  @include('website.includes.header')
  <!--End mainmenu area-->
  <!-- END HEADER -->
			<!-- CONTENT -->
			<!-- Intro Section -->
			<section class="inner-intro  padding bg-img1 overlay-dark light-color">
				<div class="container">
					<div class="row title">
						<h1>Portfolio</h1>
						<div class="page-breadcrumb">
							<a>Home</a>/<a>Portfolio</a>/<span>Lightbox</span>
						</div>
					</div>
				</div>
			</section>
			<!-- End Intro Section -->
			<!-- Work Section -->
			<section id="work" class="padding">
				<div class="container">
					<!-- work Filter -->
					<div class="row">
						<ul class="container-filter categories-filter">
							<li>
								<a class="categories active" data-filter="*">All</a>
							</li>



 @foreach($cagallery as $cagallerys)
							<li>
								<a class="categories" data-filter=".{{$cagallerys->id}}">{{$cagallerys->name}}</a>
							</li>

               @endforeach
						</ul>
					</div>
					<!-- End work Filter -->
					<div class="row container-grid nf-col-3">
 @foreach($gallerys as $gallery)
 <div class="nf-item {{ $gallery->cgallerys()->first()->id}} spacing">

    <div class="item-box">
      <a> <img alt="1" src="{{Request::root()}}/public/uploads/slider/{{$gallery->photo}}" class="item-container"> </a>
      <div class="link-zoom">
        <a href="{{Request::root()}}/public/uploads/slider/{{$gallery->photo}}" class="project_links"> <i class="fa fa-link"> </i> </a>
        <a href="{{Request::root()}}/public/uploads/slider/{{$gallery->photo}}" class="fancylight" data-fancybox-group="light" > <i class="fa fa-search-plus"></i> </a>
      </div>
      <div class="gallery-heading">
        <h4><a href="#">{{ $gallery->title}}</a></h4>
        <p>
        {{ $gallery->description}}
        </p>
      </div>
    </div>

  </div>

			 @endforeach

				</div>

			</section>
			<!--End Contact-->
			<!-- FOOTER -->
			@include('website.includes.footer')
			<!-- END FOOTER -->
		</div>
		<!-- Site Wraper End -->


		@include('website.includes.js')

		<script src="{{ asset('assets/website/js/jquery.mousewheel-3.0.6.pack.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/website/js/jquery.fancybox.pack.js') }}" type="text/javascript"></script>

	</body>

<!-- you  from theembazaar.com/demo/themejio/spaceradius/portfolio-grid.html by you  Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 08:21:25 GMT -->
</html>
