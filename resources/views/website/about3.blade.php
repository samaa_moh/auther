﻿<!DOCTYPE html>
<html lang="en">

<!-- you  from theembazaar.com/demo/themejio/spaceradius/about3.html by you  Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 08:12:21 GMT -->
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title> almasria4ceiling / about </title>
		<!-- Favicone Icon -->
		@include('website.includes.css')
	</head>

<body>
<!--loader-->
<div id="preloader">
  <div class="sk-circle">
    <div class="sk-circle1 sk-child"></div>
    <div class="sk-circle2 sk-child"></div>
    <div class="sk-circle3 sk-child"></div>
    <div class="sk-circle4 sk-child"></div>
    <div class="sk-circle5 sk-child"></div>
    <div class="sk-circle6 sk-child"></div>
    <div class="sk-circle7 sk-child"></div>
    <div class="sk-circle8 sk-child"></div>
    <div class="sk-circle9 sk-child"></div>
    <div class="sk-circle10 sk-child"></div>
    <div class="sk-circle11 sk-child"></div>
    <div class="sk-circle12 sk-child"></div>
  </div>
</div>
<!--loader-->
<!-- Site Wraper -->
<div class="wrapper">
  <!-- HEADER -->
  <!--Start header area-->
  	@include('website.includes.header')
  <!--End mainmenu area-->
  <!-- END HEADER -->
  <!-- CONTENT -->
  <!-- Intro Section -->
  <section class="inner-intro bg-img light-color overlay-before parallax-background">
    <div class="container">
      <div class="row title">
        <h1 data-title="About"><span>About</span></h1>
      </div>
    </div>
  </section>
  <!-- Intro Section -->
 <!-- About Section -->
			<div id="about-section" class="padding pt-xs-60">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="heading-box pb-30">
								<h2><span>Company</span> Overview</h2>

							</div>
						</div>
					</div>
					<div class="row">

						@foreach($abouts as $about)
							<div class="col-sm-6 pb-xs-30">
								<div class="text-content">
									<p> {{$about->description}} </p>
								</div>

							</div>
							<div class="col-sm-6">
								<img class="img-responsive" src="{{Request::root()}}/public/uploads/slider/{{$about->image}}" alt="Photo">
							</div>
						@endforeach

					</div>

				</div>
			</div>
			<!-- About Section End-->
			

			<!-- Mission Section -->
			<div id="mission-section" class="padding ptb-xs-60 bg-color light-color">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 pb-xs-30">
							<div class="heading-box pb-15">
								<h2><span>Our</span> Mission</h2>

							</div>
							<p>
								Perspiciatis unde omnis iste natus doxes sit voluptatem accusantium dantiumeaque ipsa quae ab illos Perspiciatis unde omnis iste natus doxes sit voluptatem accusantium dantiumeaque ipsa quae ab illos
							</p>

						</div>

						<div class="col-sm-6">
							<div class="heading-box pb-15">
								<h2><span>Our</span> Philosphy</h2>

							</div>
							<p>
								Perspiciatis unde omnis iste natus doxes sit voluptatem accusantium dantiumeaque ipsa quae ab illos Perspiciatis unde omnis iste natus doxes sit voluptatem accusantium dantiumeaque ipsa quae ab illos
							</p>

						</div>
					</div>

				</div>
			</div>
			<!-- Testimonials -->
			<!-- Testimonial -->
			<section class="testimonial-section padding ptb-xs-60 ">
				<div class="container">
					<div class="row text-center pb-30">
						<div class="col-sm-12">
							<div class="heading-box text-center">
								<h2><span>Our</span> Team </h2>

							</div>

						</div>
					</div>
					<div class="row">

						<div class="carousel-slider nf-carousel-theme arrow_theme light-color">

							@foreach($team as $member)
								<div class="carousel-item1">
									<div class="testimonial-block bg-color">
										<figure class="testimonial-img">
											<img class="img-circle img-border" src="{{Request::root()}}/public/uploads/slider/{{$member->photo}}" alt="">
										</figure>
										<center>
										<h3 class="testimonial-author"> {{$member->title}} </h3>
										<hr class="small-divider">
										<p> {{$member->name}} </p>
										<span class="star"> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> </span>
										</center>
									</div>
								</div>
							@endforeach

						</div>
					</div>
				</div>
			</section>
			<!-- Testimonial -->
 <!-- FOOTER -->
			 @include('website.includes.footer')
<!-- END FOOTER -->
</div>

<!-- Site Wraper End -->

		@include('website.includes.js')
			
</body>

<!-- you  from theembazaar.com/demo/themejio/spaceradius/about3.html by you  Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 08:12:21 GMT -->
</html>
