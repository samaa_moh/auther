﻿<!DOCTYPE html>
<html lang="en">

<!-- you  from theembazaar.com/demo/themejio/spaceradius/services.html by you  Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 08:15:40 GMT -->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title> almasria4ceiling / Services </title>
		@include('website.includes.css')
	</head>
	<body>
		<!--loader-->
		<div id="preloader">
			<div class="sk-circle">
				<div class="sk-circle1 sk-child"></div>
				<div class="sk-circle2 sk-child"></div>
				<div class="sk-circle3 sk-child"></div>
				<div class="sk-circle4 sk-child"></div>
				<div class="sk-circle5 sk-child"></div>
				<div class="sk-circle6 sk-child"></div>
				<div class="sk-circle7 sk-child"></div>
				<div class="sk-circle8 sk-child"></div>
				<div class="sk-circle9 sk-child"></div>
				<div class="sk-circle10 sk-child"></div>
				<div class="sk-circle11 sk-child"></div>
				<div class="sk-circle12 sk-child"></div>
			</div>
		</div>

		<!--loader-->
		<!-- HEADER -->
		<!--Start header area-->
		<!--Header Section Start Here
		==================================-->
		@include('website.includes.header')
		<!--Header End Here-->
		<!--End mainmenu area-->
		<!-- END HEADER -->
		<!-- Intro Section -->
		<section class="inner-intro bg-img light-color overlay-before parallax-background">
			<div class="container">
				<div class="row title">
					<div class="title_row">
						<h1 data-title="Services"><span>Services</span></h1>
						<div class="page-breadcrumb">
							<a>Home</a>/ <span>Services</span>
						</div>

					</div>

				</div>
			</div>
		</section>
		<!-- Intro Section -->
		
		<!-- Service Section -->
		<section id="service" class="padding ptb-xs-40">
			<div class="container">
				<div class="row text-center mb-40">
					<div class="col-lg-6 offset-lg-3 sect-title">
						<h2><span>Our</span> Service</h2>
						<span class="title_bdr"><i class="ion-more"></i></span>
					</div>
				</div>

				<div class="row">
					
					@foreach($services as $service)
						<!--Services Block-->
						<div class="services-block col-lg-4 col-md-6 mb-60 mb-xs-30">
							<div class="about-block img-scale clearfix">
								<figure>	
									<a href="{{Request::root()}}/public/uploads/slider/{{$service->photo}}">
										<img class="img-responsive" src="{{Request::root()}}/public/uploads/slider/{{$service->photo}}" alt="Photo">
									</a>
								</figure>
								<div class="text-box mt-25">
									<div class="box-title mb-15">
										<h3> {{$service->title}} </h3>
									</div>
									<div class="text-content">
										<p> {{$service->description}} </p>
										
									</div>
								</div>
							</div>
						</div>
					@endforeach

				</div>
			</div>
		</section>
		<!-- Service Section end -->

		<!-- FOOTER -->
		@include('website.includes.footer')
		<!-- END FOOTER -->

		@include('website.includes.js')

	</body>

<!-- you  from theembazaar.com/demo/themejio/spaceradius/services.html by you  Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 08:15:40 GMT -->
</html>
