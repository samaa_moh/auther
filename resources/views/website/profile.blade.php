<!DOCTYPE html> 
<html lang="en">
    <head>
        <meta charset="utf-9" />

        <title>Eddit Profile</title>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" />


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>


       
   @include('website.includes.css')

    </head>
    <body>
 
@include('website.includes.header')


        <!---Section-------->
        <section class="w-100 py-5 main_screen ">

 


            <div class="container clearfix px-0 px-md-3">
                <div class="de_full de_view">
                    
                   
                    <!-------Script Code !!!------------>
                    <div  class="col-12 col-md-12 col-lg-6 col-xl-4 g_one"></div>
                    <!-------Script Code !!!------------>
                    
                    
                    <div class="col-12 pb-5 px-0 px-md-3">
                        <div class="inp_pub py-0 row mx-0">

                         

                            <form class="col-12 py-3  col-md-12 mx-auto col-lg-7" action="{{url('my/editprofile')}}" method="post"  id="upload_form" enctype="multipart/form-data" >




 



                     <!-- Script -->
                          <div id="errors_lan" >
                            
                             </div>
                                        
                                        <!--     <strong>{{ $errors->first('imgInp') }}</strong> -->
                                  
                           


                              
                                {{csrf_field()}}
                                <div class="user-pro">
                                         <input type="text" class="d-none" id="userid" value="{{ Auth::user()->id }}">
                <img src="{{Request::root()}}/public/uplodes/userimage/{{$profiloes->first()->image}}" />


                                    <img class="upded_img" id='img-upload'/>
                                    <div class="input-group">
                                        <label>Upload Image</label>
                                        <span class="btn btn-default btn-file">
                                       


                                               <input type="file" name="imgInp" id="imgInp" >
                                        </span>
                                    </div>
                                </div>



                                <div class="form-group for_ed_user lock_dt3" id="a_p_1" onclick="an_padding('a_p_1', 'b_t_1')">
                                    <label for="title_pro"  id="b_t_1" ><i class="fab fa-black-tie"></i>Title :</label>
                                    <input type="text" name="title" class="form-control" id="title_pro" value="{{$profiloes->first()->title}}">
                                </div>


                                <div class="form-group for_ed_user lock_dt3" id="a_p_2" onclick="an_padding('a_p_2', 'b_t_2')">
                                        <label for="first_pro"  id="b_t_2" ><i class="fas fa-address-card"></i>First name :</label>
                                        <input type="text" name="firstname" class="form-control" id="first_pro" value="{{$profiloes->first()->firstname}}">
                                </div>          
                                
                                

                                <div class="form-group for_ed_user lock_dt3" id="a_p_3" onclick="an_padding('a_p_3', 'b_t_3')">
                                        <label for="middle_pro"  id="b_t_3" ><i class="fas fa-dna"></i>Middle name : </label>
                                        <input type="text" name="middlename" class="form-control" id="middle_pro"
                                        value="{{$profiloes->first()->middlename}}">
                                </div>   
                                
                                

                                <div class="form-group for_ed_user lock_dt3" id="a_p_4" onclick="an_padding('a_p_4', 'b_t_4')">
                                        <label for="last_pro"  id="b_t_4" ><i class="fas fa-dna"></i>Last name :</label>
                                        <input type="text" name="lastname" class="form-control" id="last_pro" value="{{$profiloes->first()->lastname}}">
                                </div>    
                                
                                <div class="form-group for_ed_user lock_dt3" id="a_p_5" onclick="an_padding('a_p_5', 'b_t_5')">
                                        <label for="last_pro"  id="b_t_5"  ><i class="fas fa-briefcase"></i>Role :</label>
                                    
                                        <select name="roles"  class="form-control"  >


                                        <option value="{{@$profiloes_Roles->first()->get_role()->first()->id}}" selected="selected">{{@$profiloes_Roles->first()->get_role()->first()->title}}</option>

                                                  @foreach($Roles as $role)

                                                 

                                <option   value="{{@$role->id}}">{{@$role->title}}</option>

                                                                        @endforeach
            

                                        

                                          
                                                                       

                                        </select>
                                </div>  

                                <div class="form-group for_ed_user lock_dt3" id="a_p_6" onclick="an_padding('a_p_6', 'b_t_6')">
                                        <label for="last_pro"  id="b_t_6" ><i class="fas fa-flag"></i>Subject area :</label>
                                        <select name="subjectareas_id" class="form-control" id="last_pro" >
                                            
                                            <option value="{{$profiloes->first()->subjectareas_id}}">{{$profiloes->first()->get_subjectareas->subjectareas}}</option>
                                            
                                                 @foreach($subjectareas as $subjectarea)
                                            <option   value="{{@$subjectarea->id}}">{{@$subjectarea->subjectareas}}</option>

                                                                        @endforeach
                                                                        
                                            
                                        </select>
                                </div>  

                                <div class="form-group for_ed_user " id="a_p_7" onclick="an_padding('a_p_7', 'b_t_7')">
                                        <label for="last_pro"  id="b_t_7" ><i class="fas fa-globe-europe"></i>Country :</label>
                                        <input type="text" name="country" class="form-control" id="last_pro" value="{{$profiloes->first()->country}}">
                                </div>  
             
                                <div class="form-group for_ed_user " id="a_p_8" onclick="an_padding('a_p_8', 'b_t_8')">
                                                <label for="last_pro"  id="b_t_8" ><i class="fas fa-globe-europe"></i>Institutional affiliation :</label>
                                                <input type="text" name="institutionalaffiliation" class="form-control" id="last_pro" value="{{$profiloes->first()->institutionalaffiliation}}">
                                      
                                        </div>  
                                <div class="w-100 pt-2 text-center">
                                    <button type="submit" class="btn btn-save">Save</button>
                                </div>
                            </form>


                            </div>

                        <div class="inp_pub my-5" >
                                <form class="col-12 py-3 mx-auto col-md-12 col-lg-7 mx-auto" method="post" action="{{url('my/editfindme')}}"> 
                                      {{csrf_field()}}
                                        <h4 class="h4 head-div pb-4 text-center mt-5">How to find me</h4>
                                        <p class="text-left head-div">Enter links to your public profiles to help readers find your work.</p>
                                         
                                         
                                        <div class="form-group for_ed_user" id="a_b_1" onclick="an_padding('a_b_1', 'b_a_1')">
                                                <label for="first_pro"  id="b_a_1" ><i class="fas fa-address-card"></i>Link to FaceBook :</label>

                                                <input type="text" name="facebook" class="form-control" value="{{$findme->facebook}}" />
                                        </div>    

                                         
                                        <div class="form-group for_ed_user" id="a_b_2" onclick="an_padding('a_b_2', 'b_a_2')">
                                                <label for="first_pro"  id="b_a_2" ><i class="fas fa-address-card"></i>Link to Twitter :</label>

                                                <input type="text" name="twitter" class="form-control" value="{{$findme->twitter}}" />
                                        </div>    

                                         
                                        <div class="form-group for_ed_user" id="a_b_3" onclick="an_padding('a_b_3', 'b_a_3')">
                                                <label for="first_pro"  id="b_a_3" ><i class="fas fa-address-card"></i>Link to LinkedIn :</label>

                                                <input type="text" name="linkedin" class="form-control"  value="{{$findme->linkedin}}" />
                                        </div>    


                                        <div class="form-group for_ed_user" id="a_b_4" onclick="an_padding('a_b_4', 'b_a_4')">
                                                <label for="first_pro"  id="b_a_4" ><i class="fas fa-address-card"></i>Link to ORCID :</label>

                                                <input type="text" name="orcid" class="form-control" value="{{$findme->orcid}}">
                                        </div>    


                                        <div class="form-group for_ed_user" id="a_b_5"  onclick="an_padding('a_b_5', 'b_a_5')">
                                            <label for="title_pro"  id="b_a_5" ><i class="fab fa-researchgate"></i>Link to ResearchGate :</label>
                                            <input type="text" class="form-control" name="gate" value="{{$findme->gate}}">
                                        </div>

                                        <div class="form-group for_ed_user" id="a_b_6"  onclick="an_padding('a_b_6', 'b_a_6')">
                                            <label for="title_pro"  id="b_a_6" ><i class="fab fa-researchgate"></i>Link to Academia.edu :</label>
                                            <input type="text" class="form-control" name="academia" value="{{$findme->academia}}">
                                        </div>
    
                                        <div class="form-group for_ed_user" id="a_b_7"  onclick="an_padding('a_b_7', 'b_a_7')">
                                            <label for="title_pro"  id="b_a_7" ><i class="fab fa-researchgate"></i>Link to  Mendeley :</label>
                                        
                                            <input type="text" name="mendeley" class="form-control" value="{{$findme->mendeley}}">
                                            
                                        </div>



                                    
                                       <div class="w-100 pt-2 text-center">
                                           <button type="submit" class="btn btn-save">Save</button>
                                       </div>
                                    </form>
        
                        </div>
                    </div>
                </div>

            </div>
        </section>

 @include('website.includes.footer')
        <!-- END FOOTER -->
        <!-- End footer -->

@include('website.includes.js')
    </body> 
</html>