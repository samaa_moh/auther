﻿<!DOCTYPE html>
<html lang="en">

<!-- you  from theembazaar.com/demo/themejio/spaceradius/contact.html by you  Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 08:24:29 GMT -->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> almasria4ceiling / Contact Us </title>
    @include('website.includes.css')

  </head>

	<body>
    <!--loader-->
    <div id="preloader">
      <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
      </div>
    </div>

		<!--loader-->
		<!-- HEADER -->
		<!--Start header area-->
		<!--Header Section Start Here
		==================================-->
		@include('website.includes.header')
		<!--Header End Here-->
		<!--End mainmenu area-->
		<!-- END HEADER -->

		<!-- Intro Section -->
    <section class="inner-intro bg-img light-color overlay-before parallax-background">
        <div class="container">
          <div class="row title">
            <div class="title_row">
              <h1 data-title="Contact"><span>Contact</span></h1>
              <div class="page-breadcrumb">
                <a href="{{url('/')}}">Home</a>/ <span>Contact</span>
              </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Intro Section End-->

    <!-- Contact Section -->
    <section class="ptb ptb-xs-60">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-lg-8 offset-md-2 text-center">
            <h2>KEEP IN TOUCH</h2>
            <p class="lead"> Nullam dictum felis eu pede mollis pretium leo eget bibendum sodales augue velit cursus. tellus eget condimentum rhoncus sem quam semper libero. </p>
          </div>
        </div>

        @foreach($contacts as $contact)
          <div class="row">
            <div class="col-md-12 col-lg-12 contact pb-60 pt-30">
              <div class="row text-center">
                <div class="col-md-4 col-lg-4 pb-xs-30"> 
                  <i class="ion-android-call icon-circle pos-s"></i>
                  <a href="#" class="mt-15 i-block"> {{$contact->phone}} </a> 
                </div>
                <div class="col-md-4 col-lg-4 pb-xs-30"> 
                  <i class="ion-ios-location icon-circle pos-s"></i>
                  <p  class="mt-15"> {{$contact->address}} </p>
                </div>
                <div class="col-md-4 col-lg-4 pb-xs-30"> 
                  <i class="ion-ios-email icon-circle pos-s"></i>
                  <a href="mailto:Construc@support.com"  class="mt-15 i-block"> {{$contact->email}} </a> 
                </div>
              </div>
            </div>
          </div>
         @endforeach
       </div>

       <!-- Map Section -->
       <div class="map">
          <div class="mapouter"><div class="gmap_canvas"><iframe width="100%"height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=%D8%A7%D9%84%D9%85%D8%B5%D8%B1%D9%8A%D8%A9%20%D9%84%D9%84%D8%A7%D8%B3%D9%82%D9%81%20%D8%A7%D9%84%D9%85%D8%B9%D9%84%D9%82%D8%A9&t=&z=13&ie=UTF8&iwloc=&output=embed&z=11" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.crocothemes.net">what is a wordpress site</a></div><style>.mapouter{text-align:right;height:500px;width:100%";}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:100%";}</style></div>
		    </div>
       <!-- Map Section -->
       
      <div class="container contact-form padding pt-xs-40 mt-up">
        <div class="row">
          <div class="col-md-12 col-lg-12">
            <h4>GET IN TOUCH</h4>
            <p> Nullam dictum felis eu pede mollis pretium. </p>
            
            <!-- Contact FORM -->
            <form method="POST" action="{{url('/contactUs')}}" class="contact-form mt-45" >

              {{ csrf_field() }}
              <!-- IF MAIL SENT SUCCESSFULLY -->
              <div id="success">
                <div role="alert" class="alert alert-success"> 
                  <strong>Thanks</strong> for using our template. Your message has been sent.   
                </div>
              </div>
              <!-- END IF MAIL SENT SUCCESSFULLY -->
              <div class="row">
                <div class="col-md-6 col-lg-6">
                  <div class="form-field">
                    <input class="input-sm form-full" id="name" type="text" name="form-name" placeholder="Your Name" required>
                  </div>
                  <div class="form-field">
                    <input class="input-sm form-full" id="email" type="email" name="form-email" placeholder="Email" required >
                  </div>
                  <div class="form-field">
                    <input class="input-sm form-full" id="sub" type="text" name="form-subject" placeholder="Subject" required>
                  </div>
                </div>
                <div class="col-md-6 col-lg-6">
                  <div class="form-field">
                    <textarea class="form-full" id="message" rows="7" name="form-message" placeholder="Your Message" required>

                    </textarea>
                  </div>
                </div>
                <div class="col-md-12 col-lg-12 mt-30">
                  <input type="submit" value=" Send Message" class="btn-text" name="button">
                </div>
              </div>

                @if($errors->any())
                  <div>
                    <ul>
                      @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                      @endforeach
                    <ul>
                  </div>
                @endif

            </form>
            <!-- END Contact FORM -->

          </div>
        </div>
      </div>
    </section>
    <!-- Contact Section -->

    <!-- FOOTER -->
		  @include('website.includes.footer')
		<!-- END FOOTER -->

    <!-- Site Wraper End -->
		
	  	@include('website.includes.js')
	  	<script src="{{ asset('assets/website/js/mail.js') }}" type="text/javascript"></script>

	</body>

<!-- you  from theembazaar.com/demo/themejio/spaceradius/contact.html by you  Website Copier/3.x [XR&CO'2014], Sun, 23 Dec 2018 08:24:29 GMT -->
</html>