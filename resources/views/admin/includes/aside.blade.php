<aside class="main-sidebar">


    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('assets/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{\Illuminate\Support\Facades\Auth::guard('admin')->user()->username}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
            </li>


            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>Slider</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/slider')}}">
                                <i class="fa fa-circle-o"></i> add Slider
                            </a>
                        </li>

                </ul>
            </li>


            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>Features</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/feature')}}">
                                <i class="fa fa-circle-o"></i> add Features
                            </a>
                        </li>

                </ul>
            </li>




            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>about</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/about')}}">
                                <i class="fa fa-circle-o"></i> add About Us
                            </a>
                        </li>

                </ul>
            </li>



            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>services</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/services')}}">
                                <i class="fa fa-circle-o"></i> add services
                            </a>
                        </li>

                </ul>
            </li>



            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>project</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/project')}}">
                                <i class="fa fa-circle-o"></i> add project
                            </a>
                        </li>

                </ul>
            </li>




            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>caproject</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/caproject')}}">
                                <i class="fa fa-circle-o"></i> add caproject
                            </a>
                        </li>

                </ul>
            </li>



            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>cgallery</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/cgallery')}}">
                                <i class="fa fa-circle-o"></i> add cgallery
                            </a>
                        </li>

                </ul>
            </li>




            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>Gallery</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/gallery')}}">
                                <i class="fa fa-circle-o"></i> add Gallery
                            </a>
                        </li>

                </ul>
            </li>

            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>Team</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/team')}}">
                                <i class="fa fa-circle-o"></i> add Team
                            </a>
                        </li>

                </ul>
            </li>

            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>Blog</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/blogs')}}">
                                <i class="fa fa-circle-o"></i> add Blog
                            </a>
                        </li>

                </ul>
            </li>

            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>Contact</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/contacts')}}">
                                <i class="fa fa-circle-o"></i> add Contact
                            </a>
                        </li>

                </ul>
            </li>

            <li class="treeview">
                <a href="{{url( LaravelLocalization::setLocale().'/admin/manager')}}">
                    <i class="fa fa-language"></i>
                    <span>People Contact us</span>
                    <span class="pull-right-container">
            </span>
                </a>
                <ul class="treeview-menu">
                        <li>
                            <a rel="alternate" href="{{url( LaravelLocalization::setLocale().'/admin/contactUs')}}">
                                <i class="fa fa-circle-o"></i> show Contacts
                            </a>
                        </li>

                </ul>
            </li>

    </section>
    <!-- radio -->

    <!-- /.sidebar -->
</aside>
