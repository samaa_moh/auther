@extends('admin.layouts.master')
@section('title')
    Edit team
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
            edit team
            <small></small>
        </h1>

    </section>
@endsection

@section('content')

    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="post" action="{{url('/admin/team/'.$slider->id)}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="patch">

                        <div class="box-body">

                        <div class="form-group">
                          <label for="title" class="col-sm-1 control-label">Name</label>
                          <div class="col-sm-5 {{ $errors->has('title') ? ' has-error' : '' }}">
                              <input type="text" name="name" class="form-control" id="title" placeholder="name" value="{{$slider->name}}" required autofocus>
                              @if ($errors->has('name'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('name') }}</strong>
                                  </span>
                              @endif
                          </div>
                            </div>


                            <div class="form-group">
                                <label for="title" class="col-sm-1 control-label">Title</label>
                                <div class="col-sm-5 {{ $errors->has('title') ? ' has-error' : '' }}">
                                    <input type="text" name="title" class="form-control" id="title" placeholder="Title" value="{{$slider->title}}" required autofocus>
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                    </div>

                                <div class="form-group">
                                <label for="photo" class="col-sm-1 control-label">Photo</label>
                                <div class="col-sm-5 {{ $errors->has('photo') ? ' has-error' : '' }}">
                                    <input type="file" name="photo" id="photo" class="form-control">
                                    @if ($errors->has('photo'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('photo') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                </div>


                                <div class="form-group">

                                    <label for="description" class="col-sm-1 control-label">facbook</label>
                                    <div class="col-sm-11 {{ $errors->has('description') ? ' has-error' : '' }}">
                                        <div class="box-body pad">
                                            <textarea name="facbook" class="form-control" placeholder="facbook" > {{$slider->facbook}}"</textarea>
                                            @if ($errors->has('facbook'))
                                                <span class="help-block">
                                                        <strong>{{ $errors->first('facbook') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>



                                <div class="form-group">

                                    <label for="description" class="col-sm-1 control-label">gmail</label>
                                    <div class="col-sm-11 {{ $errors->has('description') ? ' has-error' : '' }}">
                                        <div class="box-body pad">
                                            <textarea name="gmail" class="form-control" placeholder="gmail" >{{$slider->gmail}}</textarea>
                                            @if ($errors->has('gmail'))
                                                <span class="help-block">
                                                        <strong>{{ $errors->first('gmail') }}</strong>
                                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>





                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info center-block">Save <i class="fa fa-save" style="margin-left: 5px"></i></button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
                <!-- general form elements disabled -->

                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('css')

@endsection

@section('js')
    <script src="{{ asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>

@endsection
