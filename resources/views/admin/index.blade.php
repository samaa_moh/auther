@extends('admin.layouts.master')
@section('title')
Admin
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
<!--Home Page-->
            <small></small>
        </h1>

    </section>
@endsection

@section('content')

<!-- Main content -->

        <section class="content">

          <!-- Small boxes (Stat box) -->

          <div class="row">

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <p>سلايدر</p>
                </div>

                <div class="icon">
                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                </div>
                <a href="{{url( LaravelLocalization::setLocale().'/admin/slider')}}" class="small-box-footer">ذهاب الى <i class="fa fa-arrow-circle-left"></i></a>
              </div>

            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <p>من نحن</p>
                </div>
                <div class="icon">
                  <i class="fa fa-user" aria-hidden="true"></i>
                </div>
                <a href="{{url( LaravelLocalization::setLocale().'/admin/about')}}" class="small-box-footer">ذهاب الى <i class="fa fa-arrow-circle-left"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <p>اراء العملاء</p>
                </div>
                <div class="icon">
                  <i class="fa fa-comments" aria-hidden="true"></i>
                </div>
                <a href="{{url( LaravelLocalization::setLocale().'/admin/contactUs')}}" class="small-box-footer">ذهاب الى <i class="fa fa-arrow-circle-left"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <p>خدمات</p>
                </div>
                <div class="icon">
                  <i class="fa fa-tasks" aria-hidden="true"></i>
                </div>
                <a href="{{url( LaravelLocalization::setLocale().'/admin/services')}}" class="small-box-footer">ذهاب الى <i class="fa fa-arrow-circle-left"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <p>الفريق</p>
                </div>
                <div class="icon">
                  <i class="fa fa-users" aria-hidden="true"></i>
                </div>
                  <a href="{{url( LaravelLocalization::setLocale().'/admin/team')}}" class="small-box-footer">ذهاب الى <i class="fa fa-arrow-circle-left"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <p>مدونة</p>
                </div>
                <div class="icon">
                  <i class="fa fa-book" aria-hidden="true"></i>
                </div>
                <a href="{{url( LaravelLocalization::setLocale().'/admin/blogs')}}" class="small-box-footer">ذهاب الى <i class="fa fa-arrow-circle-left"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <p>تواصل معنا</p>
                </div>
                <div class="icon">
                  <i class="fa fa-compress" aria-hidden="true"></i>
                </div>
                <a href="{{url( LaravelLocalization::setLocale().'/admin/contacts')}}" class="small-box-footer">ذهاب الى <i class="fa fa-arrow-circle-left"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <p>المشاريع</p>
                </div>
                <div class="icon">
                  <i class="fa fa-tasks" aria-hidden="true"></i>
                </div>
                <a href="{{url( LaravelLocalization::setLocale().'/admin/project')}}" class="small-box-footer">ذهاب الى <i class="fa fa-arrow-circle-left"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <p>الصور</p>
                </div>
                <div class="icon">
                  <i class="fa fa-users" aria-hidden="true"></i>
                </div>
                  <a href="{{url( LaravelLocalization::setLocale().'/admin/gallery')}}" class="small-box-footer">ذهاب الى <i class="fa fa-arrow-circle-left"></i></a>
              </div>
            </div><!-- ./col -->

          </div><!-- /.row -->

        </section><!-- /.content -->


        
@endsection

@section('css')

@endsection

@section('js')

@endsection
