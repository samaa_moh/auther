@extends('admin.layouts.master')
@section('title')
    Add contact
@endsection
@section('page-header')
    <section class="content-header">
        <h1>
          add contact
            <small></small>
        </h1>

    </section>
@endsection

@section('content')

    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Horizontal Form</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="post" action="{{url('/admin/contacts')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="box-body">




                            <div class="form-group">
                          <label for="title" class="col-sm-1 control-label">email</label>
                          <div class="col-sm-5 {{ $errors->has('title') ? ' has-error' : '' }}">
                              <input type="text" name="email" class="form-control" id="title" placeholder="email" value="{{ old('email') }}" required autofocus>
                              @if ($errors->has('email'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('email') }}</strong>
                                  </span>
                              @endif
                          </div>
                          </div>


 



                        <div class="form-group">
                      <label for="title" class="col-sm-1 control-label">phone</label>
                      <div class="col-sm-5 {{ $errors->has('title') ? ' has-error' : '' }}">
                          <input type="text" name="phone" class="form-control" id="title" placeholder="phone" value="{{ old('phone') }}" required autofocus>
                          @if ($errors->has('phone'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('phone') }}</strong>
                              </span>
                          @endif
                      </div>
                      </div>



                                              <div class="form-group">
                                            <label for="title" class="col-sm-1 control-label">address</label>
                                            <div class="col-sm-5 {{ $errors->has('title') ? ' has-error' : '' }}">
                                                <input type="text" name="address" class="form-control" id="title" placeholder="address" value="{{ old('address') }}" required autofocus>
                                                @if ($errors->has('address'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('address') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            </div>


                            <div class="form-group">

                                <label for="description" class="col-sm-1 control-label">facbook</label>
                                <div class="col-sm-11 {{ $errors->has('description') ? ' has-error' : '' }}">
                                    <div class="box-body pad">
                                        <textarea name="facbook" class="form-control" placeholder="facbook" >{{ old('facbook') }}</textarea>
                                        @if ($errors->has('facbook'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('facbook') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>



                            <div class="form-group">

                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info center-block">Save <i class="fa fa-save" style="margin-left: 5px"></i></button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
                <!-- general form elements disabled -->

                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('css')

@endsection

@section('js')
    <script src="{{ asset('assets/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>

@endsection
